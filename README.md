# TODO #
  * Michał musi sernik kupic (Mariusz kazał) - jakiś konkretny?
  * Wygrać zawody

# Log prac #

## 8 czerwca 2015 ##
 * naprawiona komunikacja

## 7 czerwca 2015 ##
 * przerobienie mergowania wyniku obliczeń
 * optymalizacja algorytmu brute force

## 3 czerwca 2015 ##
 * naprawa znalezionych bugów

## 27 maja 2015 ##
 * prace nad naprawą komunikacji
 * optymalizacja kodu

## 20 maja 2015 ##
 * prace nad algorytmem

## 12 maja 2015 ##
 * przedstawienie postępu prac
 * przerabianie algorytmu

## 6 maja 2015 ##
 * prace wykończeniowe nad algorytmem
 * rozpoczęćie implementacji nowej aplikacji klienckiej
 * prace nad testami jednostkowymi
 * naprawa bugów

## 29 kwietnia 2015##
 * prace nad algorytmem
 * implementacja nowych funkcjonalności

## 15 kwietnia 2015 ##
 * prace projektowe nad algorytmem
 * drobne poprawki

##8 kwietnia 2015##
  * oddanie pierwszego etapu projektu, etap działający, problemy z testami

##1 kwietnia 2015##
  * ukończenie projektu (prima aprilis)

##25 marca 2015##
  * podział obowiązków
  * komunikacja w CS
  * planowanie testów