﻿function Localizacja()
{
    
    $PATH = $MyInvocation.PSScriptRoot
    $siClientApp = New-Object System.Diagnostics.ProcessStartInfo
    $siComSer = New-Object System.Diagnostics.ProcessStartInfo
    $siCompNd = New-Object System.Diagnostics.ProcessStartInfo
    $siTskMngNd = New-Object System.Diagnostics.ProcessStartInfo

    $siClientApp.FileName = ".\Shortcuts\ClientApp.exe.lnk"
    $siComSer.FileName = ".\Shortcuts\CommunicationServer.exe.lnk"
    $siCompNd.FileName = ".\Shortcuts\ComputationalNode.exe.lnk"
    $siTskMngNd.FileName = ".\Shortcuts\TaskManagerNode.exe.lnk"

    $siClientApp.WorkingDirectory = $PATH
    $siComSer.WorkingDirectory = $PATH
    $siCompNd.WorkingDirectory = $PATH
    $siTskMngNd.WorkingDirectory = $PATH

    [int]$nrCompNd = Read-Host "Ile uruchomić computional node"
    [int]$nrTskMngNd = Read-Host "Ile uruchomić taskmanager"

    Write-Host "Uruchamiam aplikacje klienta"
	[System.Diagnostics.Process]::Start($siClientApp)
    Write-Host "Uruchamiam serwer"
    [System.Diagnostics.Process]::Start($siComSer)

    Write-Host "Uruchamiam computional node"
    for ($i=1; $i -le $nrCompNd; $i++)
    {
        Write-Host $i
        [System.Diagnostics.Process]::Start($siCompNd)
    }

    Write-Host "Uruchamiam task managera"
    for ($i=1; $i -le $nrTskMngNd; $i++)
    {
        Write-Host $i
        [System.Diagnostics.Process]::Start($siTskMngNd)
    }
    
}

Localizacja("auto")