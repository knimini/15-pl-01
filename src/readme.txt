﻿No wiec... 

Solution ma 5 projektów...

Jeden to jest nasz klaster obliczeniowy, to jest zwykla DLL'ka i pozostale projekty maja do niej referencje. W tej DLL sa wszystkie komponenty (TM, CN, CS - klient jest tylko aplikacja, nie komponentem). Kazdy komponent implementuje kontrakt przez co moga sie pozniej ze soba komunikowac (kontrakty sa w folderze contracts). CommunicationServer ma kilka kontraktow, bo kazdy komponent (i klient) jak sie z nim laczy to korzysta z innego kontraktu.
Poza tymi komponentami, w tym projekcie sa jeszcze klasy na message jakie Okulewicz zdefiniowal w xsd. Nie szukajcie plikow typu RegisterMessage.cs , bo tych nie ma. Visual tworzy je automatycznie przy buildowaniu, a utworzone przez niego pliki laduja w /obj/Debug/XsdGeneratedTypes, ale co wazniejsze te klasy sa dostepne przez namespace CommunicationServer.ContractTypes.

Cztery pozostałe to są konsolowe programy, i tam nie ma dużo logiki. Chciałem tylko zobaczyć czy to działa :)) Ogólnie, to chyba jutro poczytam jak sie do tego unit testy pisze, bo nie wiem.
CommunicationServer : tylko uruchamia server i blokuje sie na Console.Readline(),
ComputationalNode, TaskManagerNode : wysylaja RegisterMessage do CommunicationServer i tez sie blokuja na ReadLine()
ClientApp : udaje ze wysyla problem i odbiera rozwiazanie
Cale laczenie sie jest ustawione w plikach 'app.config'. W CommunicationServer 'app.config' ma endpointy jakie eksponuje serwer, a pozostale projekty maja w 'app.config' endpoint do ktorego sie lacza. 

Ja sobie zrobilem jeszcze folder 'Shortcuts' do tych 4 execow, co by latwiej to uruchamiac. Najpierw odpalacie CommunicationServer.exe a potem pozostale i wszystko gra, chociaz wiele tego nie ma jeszcze :)

