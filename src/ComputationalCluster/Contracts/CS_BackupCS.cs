﻿using ComputationalCluster.ContractTypes;
using System;
using System.ServiceModel;

namespace ComputationalCluster.Contracts
{
    [ServiceContract(
        ConfigurationName = "BackupCSContract")]
    public interface IBackupCS : IComponent
    {
        [OperationContract(IsOneWay = true)]
        void SendSolveRequest(SolveRequest msg);

        [OperationContract(IsOneWay = true)]
        void SendRegisterRequest(Register msg);

        [OperationContract(IsOneWay = true)]
        void SendSolutions(Solutions msg);
    }

    [ServiceContract(
        CallbackContract = typeof(IBackupCS),
        ConfigurationName = "CSforBackupCS")]
    public interface ICSforBackupCS : ICSforComponent
    {
    }
}
