﻿using System;
using ComputationalCluster.ContractTypes;
using System.ServiceModel;

namespace ComputationalCluster.Contracts
{
    [ServiceContract(
        ConfigurationName="TMContract")]
    public interface ITaskManager : IComponent
    {
        [OperationContract(IsOneWay = true)]
        void DivideProblem(DivideProblem msg);

        [OperationContract(IsOneWay = true)]
        void SendSolutions(Solutions msg);

        Solutions MergeSolutions(Solutions msg);
    }

    [ServiceContract(
        CallbackContract=typeof(ITaskManager),
        ConfigurationName="CSforTMContract")]
    public interface ICSforTaskManager : ICSforComponent
    {
        [OperationContract(IsOneWay = true)]
        void SendPartialProblems(SolvePartialProblems msg);

        [OperationContract(IsOneWay = true)]
        void SendSolutions(Solutions msg);
    }
}
