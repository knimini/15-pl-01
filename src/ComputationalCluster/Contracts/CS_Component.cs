﻿using ComputationalCluster.ContractTypes;
using System;
using System.ServiceModel;

namespace ComputationalCluster.Contracts
{
    [ServiceContract]
    public interface IComponent
    {
        [OperationContract(IsOneWay = true)]
        void SendNoOperationMsg(NoOperation msg);

        [OperationContract(IsOneWay = true)]
        void SendRegisterResponse(RegisterResponse msg);
    }

    [ServiceContract(CallbackContract=typeof(IComponent))]
    public interface ICSforComponent
    {
        [OperationContract(IsOneWay = true)]
        void SendRegisterRequest(Register msg);

        [OperationContract(IsOneWay = true)]
        void SendStatus(Status msg);
    }
}
