﻿using ComputationalCluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ComputationalCluster.Contracts
{
    [ServiceContract(ConfigurationName = "CSforClientContract")]
    public interface ICSforClient
    {
        [OperationContract(IsOneWay = false)]
        SolveRequestResponse RequestProblemSolving(SolveRequest msg);

        [OperationContract(IsOneWay = false)]
        Solutions RequestSolution(SolutionRequest msg);
    }
}
