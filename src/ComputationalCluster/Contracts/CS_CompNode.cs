﻿using ComputationalCluster.ContractTypes;
using System;
using System.ServiceModel;

namespace ComputationalCluster.Contracts
{
    [ServiceContract(
        ConfigurationName = "CNContract")]
    public interface IComputationalNode : IComponent
    {
        [OperationContract(IsOneWay = true)]
        void SendPartialProblems(SolvePartialProblems msg);
    }

    [ServiceContract(
        CallbackContract = typeof(IComputationalNode),
        ConfigurationName = "CSforCNContract")]
    public interface ICSforComputationalNode : ICSforComponent
    {
        [OperationContract(IsOneWay = true)]
        void SendSolutions(Solutions msg);
    }
}
