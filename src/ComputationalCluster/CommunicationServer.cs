﻿using ComputationalCluster.Contracts;
using ComputationalCluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Linq;
using System.ServiceModel.Dispatcher;

namespace ComputationalCluster
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
    public class CommunicationServer : ClasterComponent, ICSforTaskManager, ICSforBackupCS, ICSforClient, ICSforComputationalNode, ICSforComponent, IBackupCS
    {
        private const int TIMEOUT = 10000;

        private RegisterList<Component> _components = new RegisterList<Component>();
        private RegisterList<Solve> _problems = new RegisterList<Solve>();
        private RegisterList<PartialSolve> _partialProblems = new RegisterList<PartialSolve>();
        private Dictionary<ulong, Dictionary<ulong, SolutionsSolution>> _partialResults = new Dictionary<ulong, Dictionary<ulong, SolutionsSolution>>();
        private Dictionary<ulong, Solutions> _results = new Dictionary<ulong, Solutions>();
        private EndpointDispatcher _backup;
        private IBackupCS _backupBackChannel;
        public bool IsBackup { get; set; }

        #region Registering, status, no op (from cluster component)

        public void SendRegisterRequest(Register msg)
        {
            ulong assignedId;

            if (IsBackup)
            {
                Console.WriteLine("NEW COMPONENT FROM MAIN SERVER: {0} id {1}", msg.Type.ToString(), msg.Id);
                _components.Add(new Component(msg), msg.Id);

                if (msg.Type == RegisterType.TaskManager)
                {
                    _partialResults[msg.Id] = new Dictionary<ulong, SolutionsSolution>();
                }
            }
            else
            {
                var newComponent = new Component(msg);
                object callbackChannel; /* component-type specific callback channel */

                Console.WriteLine("NEW COMPONENT : {0} id {1}", msg.Type.ToString(), msg.Id);
                
                // send back register response
                if (_backupBackChannel != null)
                {
                    _backupBackChannel.SendRegisterRequest(msg);
                    Console.WriteLine("Sent to BS new component register");
                }

                if (msg.Type == RegisterType.TaskManager)
                {
                    callbackChannel = OperationContext.Current.GetCallbackChannel<ITaskManager>();
                    _partialResults[msg.Id] = new Dictionary<ulong, SolutionsSolution>();
                }
                else if (msg.Type == RegisterType.CommunicationServer)
                {
                    Console.WriteLine("New backup server registration");
                    callbackChannel = OperationContext.Current.GetCallbackChannel<IBackupCS>();

                    foreach (var c in _components)
                    {
                        if (c.Key != msg.Id) _backupBackChannel.SendRegisterRequest(c.Value.RegisteringMsg);
                    }
                    foreach (var pr in _partialResults)
                    {
                        foreach (var presult in pr.Value)
                            _backupBackChannel.SendSolutions(new Solutions
                                {
                                    ProblemType = presult.Value.Type.ToString(),
                                    Id = presult.Value.TaskId,
                                    CommonData = presult.Value.Data,
                                    Solutions1 = new SolutionsSolution[]{
                                        new SolutionsSolution
                                        {
                                            TaskId = presult.Value.TaskId,
                                            TimeoutOccured = false,
                                            Type = SolutionsSolutionType.Partial,
                                            ComputationsTime = presult.Value.ComputationsTime,
                                            Data = presult.Value.Data
                                        }
                                    }
                                });
                    }
                }
                else /* computational node */
                {
                    callbackChannel = OperationContext.Current.GetCallbackChannel<IComputationalNode>();
                }

                /* store new component */
                assignedId = _components.Add(newComponent);
                newComponent.RegisteringMsg = msg;
                newComponent.CallbackChannel = callbackChannel;

                IComponent component = OperationContext.Current.GetCallbackChannel<IComponent>();
                component.SendRegisterResponse(new RegisterResponse()
                {
                    Id = assignedId,
                    Timeout = TIMEOUT,
                    BackupCommunicationServers = null
                });
            }
        }

        public void SendStatus(Status msg)
        {
            if (!_components.ContainsKey(msg.Id))
            {
                Console.WriteLine("Status from Id " + msg.Id + " recieved. No such component in register");
                return;
            } 
            try
            {
                _components[msg.Id].RefreshTimeout();
                Console.WriteLine("Otrzymalem status od {0}", msg.Id);
                _components[msg.Id].LastStatus = msg;
                if (_components[msg.Id].ComponentType == RegisterType.TaskManager)
                {
                    if (_partialResults[msg.Id].Count > 0)
                    {
                        if (SearchAndSendSolutions(msg)) return;
                    }
                    if (SendProblemToTM(msg)) return;
                }
                if (_components[msg.Id].ComponentType == RegisterType.ComputationalNode)
                {
                    if (SendPartialProblemToCN(msg)) return;
                }
                IComponent component = OperationContext.Current.GetCallbackChannel<IComponent>();
                component.SendNoOperationMsg(new NoOperation
                {
                    BackupCommunicationServers = null
                });
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(msg.Id);
            }
        }

        private bool SendProblemToTM(Status msg)
        {
            var minThreads = _components.Min((pair) =>
            {
                return (pair.Value.ComponentType == RegisterType.TaskManager) ? pair.Value.LastStatus.Threads.Length : int.MaxValue;
            });
            if (minThreads == msg.Threads.Length)
            {
                Solve freeSolve=null;
                foreach (var solve in _problems)
                {
                    if (!solve.Value.IsComputing())
                    {
                        freeSolve = solve.Value;
                        break;
                    }
                }
                if (freeSolve != null)
                {
                    ITaskManager taskManager = OperationContext.Current.GetCallbackChannel<ITaskManager>();
                    var nodesCount = (ulong)_components.Count((pair) =>
                        {
                            return pair.Value.ComponentType == RegisterType.ComputationalNode;
                        });
                    freeSolve.SendToTM(taskManager, 3, msg.Id);
                    return true;
                }
            }
            return false;
        }
        private bool SendPartialProblemToCN(Status msg)
        {
            var minThreads = _components.Min((pair) =>
            {
                return (pair.Value.ComponentType == RegisterType.ComputationalNode) ? pair.Value.LastStatus.Threads.Length : int.MaxValue;
            });
            if (minThreads == msg.Threads.Length)
            {
                PartialSolve freeSolve = null;
                foreach (var solve in _partialProblems)
                {
                    if (!solve.Value.IsComputing())
                    {
                        freeSolve = solve.Value;
                        break;
                    }
                }
                if (freeSolve != null)
                {
                    var compNode = OperationContext.Current.GetCallbackChannel<IComputationalNode>();
                    var nodesCount = (ulong)_components.Count((pair) =>
                    {
                        return pair.Value.ComponentType == RegisterType.ComputationalNode;
                    });
                    freeSolve.SendToCN(compNode, 3, msg.Id);
                    return true;
                }
            }
            return false;
        }
        private bool SearchAndSendSolutions(Status msg)
        {
            KeyValuePair<ulong, Solve> problem;
            try
            {
                problem = _problems.First((e) =>
                {
                    return e.Value.TM == msg.Id && e.Value.IsFinal;
                });
            }
            catch (InvalidOperationException e)
            {
                return false;
            }
            var IDs=problem.Value.GetIDs;
            var results=new List<SolutionsSolution>(IDs.Count);
            foreach(var ID in IDs){
                results.Add(_partialResults[msg.Id][ID]);
                _partialResults[msg.Id].Remove(ID);
            }
            ITaskManager taskManager = OperationContext.Current.GetCallbackChannel<ITaskManager>();
            taskManager.SendSolutions(new Solutions
            {
                Solutions1 = results.ToArray(),
                CommonData = problem.Value.GetProblemData(),
                Id = problem.Key,
                ProblemType = problem.Value.GetProblemType()
            });
            Console.WriteLine("Sent partial solutions. Problem id=" + problem.Key);
            return true;
        }

        public void SendNoOperationMsg(NoOperation msg)
        {
            Console.WriteLine("NOP");
        }

        #endregion // Registering, status, no op (from cluster component)

        #region Partial problems (from TM)

        public void SendPartialProblems(SolvePartialProblems msg)
        {
            Console.WriteLine("Mam czesciowe id={0} typ={1}", msg.Id, msg.ProblemType);
            var ids = new List<ulong>();
            for(int i=0; i<msg.PartialProblems.Length; i++)
            {
                var id=_partialProblems.Add(new PartialSolve(msg, i));
                _partialProblems[id].Id = id;
                ids.Add(id);
            }
            _problems[msg.Id].SetPartialIDs(ids);
        }

        #endregion // Partial problems (from TM)

        #region Solutions (from CN or TM)

        public void SendSolutions(Solutions msg)
        {
            Console.WriteLine("received some solutions");
            if (msg.Solutions1[0].Type == SolutionsSolutionType.Partial)
            {
                _partialResults[_problems[msg.Id].TM][msg.Solutions1[0].TaskId] = msg.Solutions1[0];

                if(_backupBackChannel != null)
                    _backupBackChannel.SendSolutions(msg);

                /* adds solution and returns true if it was last */
                if (_problems[msg.Id].SetResult(msg.Solutions1[0].TaskId))
                {
                    var tm = _components.Values.FirstOrDefault((x) => x.ComponentType == RegisterType.TaskManager);
                    if (tm == null)
                        throw new Exception("No TM connected, cannot merge");

                    // ChannelFactory<ITaskManager> dcf = new ChannelFactory<ITaskManager>("PureXMLoverTCP", tm.Address);
                    // var channel = dcf.CreateChannel();

                    var problem_solutions = _results.Values.Where((x) => x.Id == msg.Id).ToList();
                    var msgWithPartialSolutions = new Solutions();
                    msgWithPartialSolutions.Solutions1 = new SolutionsSolution[problem_solutions.Count];

                    for (int i = 0; i < problem_solutions.Count; i++)
			        {
                        var tmp = new SolutionsSolution();
                        tmp.Data = problem_solutions.ElementAt(i).Solutions1[0].Data;
                        msgWithPartialSolutions.Solutions1[i] = tmp;
			        }

                    /* send to first TM to merge */
                    var tmEntry = _components.Values.First((entry) => entry.ComponentType == RegisterType.TaskManager);
                    var tmChannel = tmEntry.CallbackChannel as ITaskManager;
                    var mergedSolutions = tmChannel.MergeSolutions(msgWithPartialSolutions);

                    /* store merged results */
                    _results[msg.Id] = mergedSolutions;
                }
            }
            else
            {
                _results[msg.Id] = msg;
            }
        }

        #endregion // Solutions (from CN)

        #region Solve / solution requests (from client)

        public SolveRequestResponse RequestProblemSolving(SolveRequest msg)
        {
            Console.WriteLine("Requested new problem to solve");
            if (!IsBackup)
            {
                msg.Id = _problems.Add(new Solve(msg));

            }
            return new SolveRequestResponse()
            {
                Id=msg.Id
            };
        }

        public Solutions RequestSolution(SolutionRequest msg)
        {
            Console.WriteLine("Solution requested for problem id: {0}", msg.Id);
            
            if(_results.ContainsKey(msg.Id))
                return _results[msg.Id];
            else
            {
                Console.WriteLine("Solution is not yet ready");
                return new Solutions
                {
                    Id = 0
                };
            }
        }

        #endregion Solve / solution requests (from client)


        void IBackupCS.SendSolveRequest(SolveRequest msg)
        {
            throw new NotImplementedException();
        }
    }
}
