﻿using ComputationalCluster.Contracts;
using ComputationalCluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.DVRP;

namespace ComputationalCluster
{
    public class ComputationalNode : ClasterComponent, IComputationalNode
    {
        public void SendPartialProblems(SolvePartialProblems msg)
        {
            //var ts = new DvrpSolver(msg.CommonData);
            var t1=DateTime.Now;
            //var result=ts.Solve(msg.CommonData, TimeSpan.FromSeconds((double)msg.SolvingTimeout));
            var result = msg.PartialProblems[0].Data.Sum((e) =>
            {
                return (short)e;
            });
            var channel = OperationContext.Current.GetCallbackChannel<ICSforComputationalNode>();
            channel.SendSolutions(new Solutions
            {
                ProblemType = msg.ProblemType,
                Id = msg.Id,
                CommonData = msg.CommonData,
                Solutions1 = new SolutionsSolution[]{
                    new SolutionsSolution
                    {
                        TaskId = msg.PartialProblems[0].TaskId,
                        TimeoutOccured = false,
                        Type = SolutionsSolutionType.Partial,
                        ComputationsTime = (ulong) (t1 - DateTime.Now).TotalMilliseconds,
                        Data = new byte[]{(byte) result}
                    }
                }
            });
        }


        public void SendNoOperationMsg(NoOperation msg)
        {
            Console.WriteLine("NOP");
        }
    }
}
