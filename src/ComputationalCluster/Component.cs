﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComputationalCluster.Contracts;
using ComputationalCluster.ContractTypes;
using System.ServiceModel;

namespace ComputationalCluster
{
    public class Component
    {
        DateTime _lastConnect;
        public object CallbackChannel { get; set; }
        public Register RegisteringMsg { get; set;}
        public Status LastStatus { get; set; }

        public ulong Id { get { return RegisteringMsg.Id; } }
        public RegisterType ComponentType { get { return RegisteringMsg.Type; } }

        public Component(Register msg)
        {
            this.RegisteringMsg = msg;
            this._lastConnect = DateTime.Now;
        }
        public void RefreshTimeout()
        {
            _lastConnect = DateTime.Now;
        }
        
        public override string ToString()
        {
            return RegisteringMsg.Type+" Id="+RegisteringMsg.Id;
        }
    }
}
