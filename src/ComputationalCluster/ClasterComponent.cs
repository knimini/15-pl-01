﻿using ComputationalCluster.Contracts;
using ComputationalCluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputationalCluster
{
    public abstract class ClasterComponent : IComponent
    {
        public ulong Id { get; set; }
        public uint Timeout { get; set; }
        public void SendNoOperationMsg(NoOperation msg)
        {
            Console.WriteLine("NOP");
        }

        public void SendRegisterResponse(RegisterResponse msg)
        {
            Id = msg.Id;
            Timeout = msg.Timeout;
            Console.WriteLine("My id is " + Id);
        }
    }
}
