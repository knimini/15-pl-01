﻿using ComputationalCluster.Contracts;
using ComputationalCluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using TaskSolvers.Utilities;
using TaskSolvers.DVRP;
using System.Linq;

namespace ComputationalCluster
{
    public class TaskManager : ClasterComponent, ITaskManager
    {
        
        public void DivideProblem(DivideProblem msg)
        {
            System.Console.Out.WriteLine("New problem to divide. Problem ID {0}, problem type {1}", msg.Id, msg.ProblemType);

            DuplexChannelFactory<ICSforTaskManager> dcf =
                new DuplexChannelFactory<ICSforTaskManager>(this, "csEndpoint" /* from app.config file */);


            //var taskSolver=new DvrpSolver(msg.Data);
            var partialProblems=new List<SolvePartialProblemsPartialProblem>((int)msg.ComputationalNodes);
            /*foreach(var pp in taskSolver.DivideProblem((int) msg.ComputationalNodes)){
                partialProblems.Add(new SolvePartialProblemsPartialProblem{
                    Data=pp,
                    TaskId=msg.Id,
                    
                });
            }*/
            var datas = new List<byte>[msg.ComputationalNodes];
            for (var i = 0; i < datas.Length; i++)
            {
                datas[i] = new List<byte>();
            }
            for (var i = 0; i < msg.Data.Length; i++)
            {
                datas[i * (int)msg.ComputationalNodes / msg.Data.Length].Add(msg.Data[i]);
            }
            for (var i = 0; i < datas.Length; i++)
            {
                partialProblems.Add(new SolvePartialProblemsPartialProblem
                {
                    Data = datas[i].ToArray(),
                });
            }
            var channel = dcf.CreateChannel();
            channel.SendPartialProblems(new SolvePartialProblems
            {
                Id = msg.Id,
                ProblemType = msg.ProblemType,
                PartialProblems=partialProblems.ToArray(),
                SolvingTimeout=500
            });
            //solutions = new byte[partialProblems.Count][];
            
            solutionsCount = 0;

            System.Console.Out.WriteLine("partial problems send");
        }

        public void SendSolutions(Solutions msg)
        {
            var dcf = new DuplexChannelFactory<ICSforTaskManager>(this, "csEndpoint" /* from app.config file */);
            var channel = dcf.CreateChannel();
            msg.Solutions1 = new SolutionsSolution[]{ new SolutionsSolution{
                ComputationsTime=(ulong) msg.Solutions1.Sum((e)=>{
                    return (long) e.ComputationsTime;
                    }),
                Data=new byte[]{ (byte) msg.Solutions1.Sum((e)=>{
                    return  e.Data[0];
                })},
                Type=SolutionsSolutionType.Final
            }};
            channel.SendSolutions(msg);
        }

        public Solutions MergeSolutions(Solutions msg)
        {
            /* convert solutions to byte[][] type */
            var solutions_data = new byte[msg.Solutions1.Length][];
            for (int i = 0; i < msg.Solutions1.Length; i++)
                solutions_data[i] = msg.Solutions1[i].Data;

            /* merge using the solver */
            var taskSolver = new DvrpBruteSolver(null);
            byte[] solution = taskSolver.MergeSolution(solutions_data);

            /* return merged solution */
            return new Solutions
            {
                CommonData = solution,
                Solutions1 = new SolutionsSolution[]{
                    new SolutionsSolution{
                        Type = SolutionsSolutionType.Final,
                        Data = solution
                    }
                }
            };
        }

        
        public void SendNoOperationMsg(NoOperation msg)
        {
            Console.WriteLine("NOP");
        }

        public int solutionsCount { get; set; }
    }
}
