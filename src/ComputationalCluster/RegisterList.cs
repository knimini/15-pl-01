﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputationalCluster
{
    class RegisterList<Q> : Dictionary<ulong, Q>
    {
        ulong _nextId;
        public RegisterList()
        {
            _nextId = 1;
        }
        public ulong Add(Q Element){
            this[_nextId]=Element;
            return _nextId++;
        }
        public ulong Add(Q Element, ulong Id)
        {
            this[Id] = Element;
            if (Id >= _nextId) _nextId = Id + 1;
            return Id;
        }
    }
}
