﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComputationalCluster.Contracts;
using ComputationalCluster.ContractTypes;

namespace ComputationalCluster
{
    class Solve
    {
        SolveRequest _solve;
        ulong _taskManager;
        List<ulong> _partialIDs;
        List<ulong> _partialIDs2;
        public Solve(SolveRequest solve)
        {
            _solve = solve;
        }
        public bool IsComputing(){
            return _taskManager!=0;
        }
        public byte[] GetProblemData()
        {
            return _solve.Data;
        }
        public String GetProblemType()
        {
            return _solve.ProblemType;
        }
        public void SendToTM(ITaskManager TMHandler, ulong NodesCount, ulong TM)
        {
            TMHandler.DivideProblem(new DivideProblem()
            {
                ProblemType = _solve.ProblemType,
                Id = _solve.Id,
                ComputationalNodes = NodesCount,
                Data = _solve.Data,
                NodeID = TM
            });
            _taskManager = TM;
        }
        public void SetPartialIDs(List<ulong> IDs)
        {
            _partialIDs = new List<ulong>(IDs);
            _partialIDs2 = new List<ulong>(IDs);
        }
        public bool SetResult(ulong id)
        {
            _partialIDs.Remove(id);

            return _partialIDs.Count == 0;
        }
        public bool IsFinal
        {
            get
            {
                return _partialIDs.Count == 0;
            }
        }
        public List<ulong> GetIDs
        {
            get{
                return _partialIDs2;
            }
        }
        public ulong TM
        {
            get
            {
                return _taskManager;
            }
        }
    }
}
