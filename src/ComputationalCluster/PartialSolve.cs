﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComputationalCluster.ContractTypes;
using ComputationalCluster.Contracts;

namespace ComputationalCluster
{
    class PartialSolve
    {
        private SolvePartialProblems _problem;
        private ulong _compNode;
        public PartialSolve(SolvePartialProblems problem, int number)
        {
            _problem = new SolvePartialProblems
            {
                CommonData = problem.CommonData,
                Id = problem.Id,
                PartialProblems = new SolvePartialProblemsPartialProblem[] { problem.PartialProblems[number] },
                ProblemType = problem.ProblemType,
                SolvingTimeout = problem.SolvingTimeout,
                SolvingTimeoutSpecified = problem.SolvingTimeoutSpecified,
            };
        }
        public bool IsComputing()
        {
            return _compNode != 0;
        }
        public void SendToCN(IComputationalNode CNHandler, ulong NodesCount, ulong CN)
        {
            CNHandler.SendPartialProblems(_problem);
            _compNode = CN;
        }
        public ulong Id
        {
            get
            {
                return _problem.PartialProblems[0].TaskId;
            }
            set
            {
                _problem.PartialProblems[0].TaskId = value;
            }
        }
    }
}
