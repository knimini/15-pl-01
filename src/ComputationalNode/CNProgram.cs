﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cluster;
using Cluster.ContractTypes;
using Cluster.Contracts;
using TaskSolvers.DVRP;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace ComputationalNode
{
    class CNProgram
    {
        static string[] solvableProblems = new string[] { "DynamicVehicleRouting" };
        static Type[] solverTypes = new Type[] { typeof(DvrpBruteSolver) };

        static readonly string EndpointName = "csEndpoint_v2_cn";

        static void Main(string[] args)
        {
            Console.WriteLine("============ COMPUTATIONAL NODE V2.0 (build 8420) ==============");
            
            /* create task solver with DvrpSolvers */
            CN cn = new CN();           
            DvrpBruteSolver bruteSolver = new DvrpBruteSolver(null);
            cn.AddTaskSolver(bruteSolver.Name, bruteSolver);

            /* create ChannelFactory and register to CS */
            var factory = new DuplexChannelFactory<ICSForCN>(cn, EndpointName);
            cn.RegisterToCS(factory);
            
            Console.ReadLine();
            cn.Shutdown();
        }

        static void PrintServiceInfo(ServiceHost host)
        {
            Console.WriteLine("{0} is up and running with these endpoints:", host.Description.ServiceType);
            foreach (ServiceEndpoint se in host.Description.Endpoints)
                Console.WriteLine(se.Address);
        }
    }
}
