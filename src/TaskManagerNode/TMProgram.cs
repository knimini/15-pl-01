﻿using System;
using System.ServiceModel;
using Cluster;
using Cluster.Contracts;
using TaskSolvers.DVRP;
using UCCTaskSolver;
using System.ServiceModel.Description;

namespace TaskManagerNode
{
    static class TMProgram
    {
        static readonly string EndpointName = "csEndpoint_v2_tm";

        static void Main(string[] args)
        {
            Console.WriteLine("============ TASK MANAGER V2.0 (service pack 18.0) ==============");
            
            /* create task solver with DvrpSolvers */
            var tm = new TM();
            TaskSolver bruteSolver = new DvrpBruteSolver(null);
            tm.AddNewSolver(bruteSolver.Name, bruteSolver);

            /* create channel and register to CS */
            var dcf = new DuplexChannelFactory<ICSForTM>(tm, EndpointName);
            tm.RegisterToCS(dcf);
            
            Console.ReadLine();
            tm.Shutdown();
        }

        static void PrintServiceInfo(ServiceHost host)
        {
            Console.WriteLine("{0} is up and running with these endpoints:", host.Description.ServiceType);
            foreach (ServiceEndpoint se in host.Description.Endpoints)
                Console.WriteLine(se.Address);
        }
    }
}
