﻿using Cluster.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using UCCTaskSolver;
using Cluster.ContractTypes;
using System.Threading;
using System.Diagnostics;

namespace Cluster
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
    public class TM : ITaskManager
    {
        private Dictionary<string, TaskSolver> _pnameToSolverMap;    // solvers for merging
        private Dictionary<string, Type> _pnameToSolverTypeMap;      // due to awesome design by sir Okulewicz, I have to 
                                                                     // instantiate new solver for every DivideProblem operation
        private ChannelFactory<ICSForTM> _csFactory;
        private int _assignedId;
        private TimeSpan _refreshInterval;

        private Task _statusSendingTask;
        private CancellationTokenSource _statusSendingTokenSource;

        public TM()
        {
            _pnameToSolverMap = new Dictionary<string, TaskSolver>();
            _pnameToSolverTypeMap = new Dictionary<string, Type>();
        }

        public void AddNewSolver(string pname, TaskSolver solver)
        {
            _pnameToSolverMap[pname] = solver;
            _pnameToSolverTypeMap[pname] = solver.GetType();
        }

        // ===== Contract implementation =====
        public Solutions MergeTasksSolutions(Solutions msg)
        {
            TaskSolver solver;
            bool hasSolver = _pnameToSolverMap.TryGetValue(msg.ProblemType, out solver);
            
            /* If no valid solver exists => return nulls */
            if (hasSolver == false)
            {
                return new Solutions()
                {
                    Id = msg.Id,
                    Solutions1 = null
                };
            }

            /* merge solutions */
            byte[][] partialSolns = msg.Solutions1.Select((ss) => ss.Data).ToArray();
            var mergedSoln = solver.MergeSolution(partialSolns);

            /* reply back */
            return new Solutions()
            {
                Id = msg.Id, /* same problem id */
                Solutions1 = new SolutionsSolution[1] 
                { 
                    new SolutionsSolution() { Data = mergedSoln } 
                }
            };
        }

        public PartialProblems DivideProblem(DivideProblem msg)
        {
            int pid = (int) msg.Id;
            string pname = msg.ProblemType;
            Console.WriteLine(String.Format("Dividing problem [pid {0}]", pid));

            Type solverType;
            bool hasSolver = _pnameToSolverTypeMap.TryGetValue(pname, out solverType);
            /* If no valid solver type exists => return nulls */
            if (hasSolver == false)
            {
                return new PartialProblems()
                {
                    Id = msg.Id,
                    PartialProblems1 = null
                };
            }

            /* divide problems */
            var solver = (TaskSolver) Activator.CreateInstance(solverType, msg.Data);
            byte[][] subproblems = solver.DivideProblem((int) msg.ComputationalNodes);

            /* reply back with divided problems */
            Console.WriteLine(String.Format("Dividing complete [pid {0}]", pid));

            var pps1 = subproblems.Select((data) =>
                    {
                        return new PartialProblemsPartialProblem()
                        {
                            Data = data,
                            TaskId = (ulong)_assignedId,
                            NodeID = 0 /* wtf is this for? */
                        };
                    }).ToArray();
            return new PartialProblems()
            {
                Id = (ulong) pid,
                ProblemType = pname,
                CommonData = new byte[]{ 0 },
                PartialProblems1 = pps1
            };
        }
    
        // ===== Public functions =====
        public void RegisterToCS(ChannelFactory<ICSForTM> csFactory)
        {
            Thread.Sleep(1000);
            _csFactory = csFactory;
            var channel = _csFactory.CreateChannel();

            var response = channel.Register(new Register()
            {
                Type = RegisterType.TaskManager,
                ParallelThreads = 0,
                SolvableProblems = _pnameToSolverMap.Keys.ToArray()
            });

            _assignedId = (int)response.Id;
            _refreshInterval = TimeSpan.FromMilliseconds(response.Timeout);

            Console.WriteLine("[Registration complete] Assigned id: " + _assignedId);
            BeginSendingStatus();
        }

        public void Shutdown()
        {
            _statusSendingTokenSource.Cancel();
            _statusSendingTask.Wait();
        }

        // ===== Private utility functions =====
        private void BeginSendingStatus()
        {
            _statusSendingTokenSource = new CancellationTokenSource();
            var ct = _statusSendingTokenSource.Token;
            _statusSendingTask = Task.Run(() =>
            {
                var channel = _csFactory.CreateChannel();
                var msg = new Status();
                msg.Id = (ulong)_assignedId;
                msg.Threads = null;
                while (ct.IsCancellationRequested == false)
                {
                    Thread.Sleep(_refreshInterval);
                    Console.WriteLine("[Refreshing status]");
                    channel.RefreshStatus(msg);
                }
            });
        }
    }
}
