﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cluster.Contracts;
using Cluster.ContractTypes;
using System.ServiceModel;
using System.Threading;
using TaskSolvers.DVRP;
using TaskSolvers.Utilities;
using System.IO;

namespace Cluster
{
    class TaskManagerEntry
    {
        public int id;                  // component id (also index into _channels list)
        public string[] problemTypes;   // problem types it can solve / merge
        public DateTime lastStatus;     // time of last status msg received from this CN

        public TaskManagerEntry(int id, string[] problemTypes)
        {
            this.id = id;
            this.problemTypes = problemTypes;
            lastStatus = DateTime.Now;
        }
    }

    class ComputationalNodeEntry
    {
        public int id;                  // component id (also index into _channels list)
        public string[] problemTypes;   // problem types this node can 
        public bool isBusy;             // True if node is calculating stuff
        public DateTime lastStatus;     // time of last status msg received from this CN

        public ComputationalNodeEntry(int id, string[] problemTypes, bool isBusy = false)
        {
            this.id = id;
            this.problemTypes = problemTypes;
            this.isBusy = isBusy;
            this.lastStatus = DateTime.Now;
        }
    }

    class BackupServerEntry
    {
        public int id;                  // component id (also index into _channels list)

        public BackupServerEntry(int id)
        {
            this.id = id;
        }
    }

    class ProblemEntry
    {
        public DateTime SubmitTime { get; set; } // problem was submitted
        public string ProblemName { get; set; } // problem type name
        public TimeSpan Timeout { get; set; }   // max compute duration specified by client
        public byte[] Solution { get; set; }    // solution ready to be returned

        public string ErrorMsg { get; set; }        // error in processing (e.g. no CNs, or no TM)
        
        public List<int> UnsolvedTids { get; set; }                     // tids that still haven't been solved
        public Dictionary<int, byte[]> PartialSolutions { get; set; }   // Partial solutions (key == tid)

        public ProblemEntry()
        {
            PartialSolutions = new Dictionary<int, byte[]>();
        }
    }

    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public class CS : ICommunicationServer
    {
        private TimeSpan _statusRefreshInterval;

        List<object> _channels;                             // list with channels for all components, addressed by it's id 
        List<TaskManagerEntry> _taskMangers;                // task managers
        List<ComputationalNodeEntry> _computationalNodes;   // compuational nodes
        List<BackupServerEntry> _backupServers;             // back servers

        object _problemsLock;
        List<ProblemEntry> _problems;               // problems being computed or waiting for client to request soln (addressed by problem id)

        List<ComputationalNodeEntry> _freeNodes;    // CNs which are ready to do the work
        object _freeNodesMonitor;                   // Monitor for _freeNodes

        // indexed by tid, returns pid
        // needed because XSD is so fucking shit that I can't send two ids (CN id, problemId) inside one msg
        object _tidMapLock;
        List<int> _tidToPidMap;          

        public CS(TimeSpan statusRefreshInterval)
        {
            _statusRefreshInterval = statusRefreshInterval;

            _channels = new List<object>();
            _taskMangers = new List<TaskManagerEntry>();
            _computationalNodes = new List<ComputationalNodeEntry>();
            _backupServers = new List<BackupServerEntry>();

            _problems = new List<ProblemEntry>();
            _problemsLock = new object();

            _freeNodes = new List<ComputationalNodeEntry>();
            _freeNodesMonitor = new object();

            _tidMapLock = new object();
            _tidToPidMap = new List<int>();
        }

        #region ICommunicationServer implementation

        public RegisterResponse Register(Register regMsg)
        {
            /* get appropertiate channel */
            object channel;
            switch (regMsg.Type)
	        {
		        case RegisterType.TaskManager:
                    channel = OperationContext.Current.GetCallbackChannel<ITaskManager>();
                    break;
                case RegisterType.ComputationalNode:
                    channel = OperationContext.Current.GetCallbackChannel<IComputationalNode>();
                    break;
                case RegisterType.CommunicationServer:
                    channel = OperationContext.Current.GetCallbackChannel<ICommunicationServer>();
                    break;
                default:
                    channel = null;
                    break;
	        }

            /* register (update internal lists) */
            int assignedId = Register(regMsg.Type, channel, regMsg);
            
            /* respond back */
            return new RegisterResponse()
            {
                Id = (ulong) assignedId,
                Timeout = (uint) _statusRefreshInterval.TotalMilliseconds
            };
        }

        public NoOperation RefreshStatus(Status statusMsg)
        {
            /* retrieve entry object */
            int componentId = (int) statusMsg.Id;
            RegisterType componentType;
            object entry;
            GetEntryFromId(componentId, out entry, out componentType);

            /* update last status field */
            switch (componentType)
            {
                case RegisterType.TaskManager:
                    (entry as TaskManagerEntry).lastStatus = DateTime.Now;
                    break;
                case RegisterType.ComputationalNode:
                    (entry as ComputationalNodeEntry).lastStatus = DateTime.Now;
                    break;
                case RegisterType.CommunicationServer:
                default:
                    break;
            }

            /* respond back with nop */
            Console.WriteLine("[status refresh] " + componentId.ToString());
            return new NoOperation()
            {
                //BackupCommunicationServers = GetBackupsForNop()
                BackupCommunicationServers=null

            };
        }

        public void SubmitPartialSolution(Solutions msg)
        {
            int cnId = (int)msg.Id;
            int tid = (int)msg.Solutions1[0].TaskId;
            int pid = _tidToPidMap[tid];
            Console.WriteLine(String.Format("Partial result arrived from {0} [tid {1}, pid {2}]", cnId, tid, pid));

            /* remove tid from _tidPidMap (so that tid value can be reused) */
            lock (_tidMapLock)
            {
                _tidToPidMap[tid] = -1;
            }

            /* mark cn as free */
            lock (_freeNodesMonitor)
            {
                var cnEntry = _computationalNodes.Single((e) => e.id == cnId);
                cnEntry.isBusy = false;
                _freeNodes.Add(cnEntry);
                Monitor.PulseAll(_freeNodesMonitor);
            }

            lock (_problemsLock)
            {
                /* remove tid from 'unsolved list' & add solution */
                int solvedTid = (int) msg.Solutions1[0].TaskId;
                var pentry = _problems[pid];
                pentry.UnsolvedTids.Remove(solvedTid);
                pentry.PartialSolutions.Add(solvedTid, msg.Solutions1[0].Data);

                /* if all tids were solved - send to merge & store result */
                if (pentry.UnsolvedTids.Count == 0)
                {
                    /* get tm to merge results */
                    Console.WriteLine(String.Format("Received all partials for a problem [pid {0}]", pid));
                    var tmEntry = _taskMangers.FirstOrDefault((e) => e.problemTypes.Contains(pentry.ProblemName));
                    if (tmEntry == null)
                    {
                        pentry.ErrorMsg = "No TM to merge the results :((";
                        Console.WriteLine(String.Format("No TM to merge problem [pid {0}]", pid));
                        return;
                    }
                    var tm = _channels[tmEntry.id] as ITaskManager;

                    /* merge results */
                    var mergeResponse = tm.MergeTasksSolutions(new Solutions()
                    {
                        Id = (ulong) pid,
                        ProblemType = pentry.ProblemName,
                        Solutions1 = pentry.PartialSolutions.Select((kvp) =>
                            {
                                return new SolutionsSolution() 
                                {
                                    TaskId = (ulong) kvp.Key,
                                    Type = SolutionsSolutionType.Partial,
                                    Data = kvp.Value
                                };
                            }).ToArray()
                    });

                    pentry.PartialSolutions = null;
                    pentry.Solution = mergeResponse.Solutions1[0].Data;

                    SaveResultToFile(_problems[pid]);
                    Console.WriteLine(String.Format("PROBLEM SOLVED! [pid {0}]", pid));
                }
            }
        }

        public Solutions RequestSolution(SolutionRequest solnRequest)
        {
            int pid = (int) solnRequest.Id;
            Console.WriteLine(String.Format("Client requested problem solution [pid {0}]", pid));

            /* there's no problem with this id */
            if (_problems.Count <= pid || pid < 0)
            {
                return new Solutions()
                {
                    Id = solnRequest.Id,
                    ProblemType = string.Empty,
                    Solutions1 = null
                };
            }
            /* problem is solved, respond with soln and remove problem */
            if (_problems[pid].Solution != null)
            {
                var response = new Solutions()
                {
                    Id = solnRequest.Id,
                    ProblemType = _problems[pid].ProblemName,
                    Solutions1 = new SolutionsSolution[] 
                    {
                        new SolutionsSolution() { Data =  _problems[pid].Solution }
                    }
                };
                _problems.RemoveAt(pid);
                return response;
            }
            /* error occured during processing, respond with null and remove problem entry */
            else if (_problems[pid].ErrorMsg != null)
            {
                var response = new Solutions()
                {
                    Id = solnRequest.Id,
                    ProblemType = _problems[pid].ProblemName,
                    Solutions1 = null
                };
                _problems.RemoveAt(pid);
                return response;
            }
            else /* problem not solved , respond null */
            {
                return new Solutions()
                {
                    Id = solnRequest.Id,
                    ProblemType = _problems[pid].ProblemName,
                    Solutions1 = null
                };
            }
        }

        public SolveRequestResponse SubmitNewProblem(SolveRequest solveRequest)
        {
            /* create problem entry */
            ProblemEntry entry = new ProblemEntry()
            {
                ProblemName = solveRequest.ProblemType,
                Timeout = TimeSpan.Zero,
                SubmitTime = DateTime.Now
            };

            /* store problem in the list */
            int assignedId;
            lock (_problemsLock)
            {
                int firstAvailable = _problems.IndexOf(null);
                if (firstAvailable == -1)
                {
                    assignedId = _problems.Count;
                    _problems.Add(entry);
                }
                else
                {
                    assignedId = firstAvailable;
                    _problems[assignedId] = entry;
                }
            }

            /* start problem solving in parallel thread */
            Task.Run(() => DivideAndSendToNodes(assignedId, solveRequest.ProblemType, solveRequest.Data));

            /* respond to client with id */
            Console.WriteLine(String.Format("New problem requested [assigned pid = {0}]", assignedId));
            return new SolveRequestResponse() { Id = (ulong) assignedId };
        }

        #endregion // ICommunicationServer implementation
        
        /// <summary>
        /// Registers component (updates inner lists) and returns assigned id. 
        /// </summary>
        private int Register(RegisterType componentType, object channel, Register regMsg)
        {
            /* put into channels list (determines id too) */
            int assignedId;
            int firstEmpty = _channels.IndexOf(null);
            if (firstEmpty == -1)
            {
                assignedId = _channels.Count;
                _channels.Add(channel);
            }
            else
            {
                assignedId = firstEmpty;
                _channels[firstEmpty] = channel;
            }

            /* put into component specific list */
            switch (componentType)
            {
                case RegisterType.TaskManager:
                    _taskMangers.Add(new TaskManagerEntry(assignedId, regMsg.SolvableProblems));
                    break;
                case RegisterType.ComputationalNode:
                    /* new free node  */
                    var newCn = new ComputationalNodeEntry(assignedId, regMsg.SolvableProblems);
                    _computationalNodes.Add(newCn);
                    lock (_freeNodesMonitor)
                    {
                        _freeNodes.Add(newCn);
                    }
                    /* pulse waiting threads after next status (to make sure that node got it's id first) */
                    Task.Run(() =>
                    {
                        var registrationTime = DateTime.Now;
                        while (newCn.lastStatus < registrationTime)
                        {
                            Thread.Sleep(_statusRefreshInterval);
                        }
                        lock (_freeNodesMonitor)
                        {
                            Monitor.PulseAll(_freeNodesMonitor);
                        }
                    });

                    break;
                case RegisterType.CommunicationServer:
                    _backupServers.Add(new BackupServerEntry(assignedId));
                    break;
                default:
                    /* roll back registration */
                    _channels[assignedId] = null;
                    assignedId = -1;
                    break;
            }

            Console.WriteLine(String.Format("New {0} registered! Assigned id: {1}!", componentType.ToString(), assignedId.ToString()));
            return assignedId;
        }

        private void GetEntryFromId(int componentId, out object entry, out RegisterType componentType)
        {
            object channel = _channels[componentId];
            
            if (channel is ITaskManager)
            {
                componentType = RegisterType.TaskManager;
                entry =  _taskMangers.First((e) => e.id == componentId);
            }
            else if (channel is IComputationalNode)
            {
                componentType = RegisterType.ComputationalNode;
                entry = _computationalNodes.First((e) => e.id == componentId);
            }
            else /* channel is ICommunicationServer */
            {
                componentType = RegisterType.CommunicationServer;
                entry = _backupServers.First((e) => e.id == componentId);
            }
        }

        private List<int> GenerateTids(int count, int pid)
        {
            List<int> result = new List<int>(count);

            lock (_tidMapLock)
            {
                for (int i = 0; i < count; i++)
                {
                    int freeIndex = _tidToPidMap.IndexOf(-1);
                    if (freeIndex == -1)
                    {
                        int newTid = _tidToPidMap.Count;
                        result.Add(newTid);
                        _tidToPidMap.Add(pid);
                    }
                    else
                    {
                        int newTid = freeIndex;
                        result.Add(newTid);
                        _tidToPidMap[freeIndex] = pid;
                    }
                }
            }
            
            return result;
        }

        private NoOperationBackupCommunicationServers GetBackupsForNop()
        {
            return new NoOperationBackupCommunicationServers()
            {

            };
        }

        /// <summary>
        /// Drives problem execution flow.
        /// divide -> send to solve
        /// </summary>
        private void DivideAndSendToNodes(int pid, string pname ,byte[] pdata)
        {
            /* count how many CNs for that problem exist */
            int nodesForProblem = _computationalNodes.Count((e) => e.problemTypes.Contains(pname));
            if (nodesForProblem == 0)
            {
                lock (_problemsLock)
                {
                    _problems[pid].ErrorMsg = "No CNs for this problem";
                }
                return;
            }

            /* find tm to divide the problem */
            var tmEntry = _taskMangers.FirstOrDefault((e) => e.problemTypes.Contains(pname));
            if (tmEntry == null)
            {
                lock (_problemsLock)
                {
                    _problems[pid].ErrorMsg = "No TM for this problem type";
                }
                return;
            }
            var tm = (_channels[tmEntry.id] as ITaskManager);

            /* divide the problem */
            var divideResponse = tm.DivideProblem(new DivideProblem()
            {
                Id = (ulong) pid,
                Data = pdata,
                ComputationalNodes = (ulong) nodesForProblem,
                ProblemType = pname
            });

            /* create tids and store them as 'unsolved' */
            var tids = GenerateTids(divideResponse.PartialProblems1.Length, pid);
            lock (_problemsLock)
            {
                _problems[pid].UnsolvedTids = tids;
            }

            /* send partials to be solved */
            var partialResults = new Dictionary<int /*tid*/, byte[]>();
            int nextPartialIndex = 0;
            int partialsCount = divideResponse.PartialProblems1.Length;
            while (nextPartialIndex < partialsCount)
            {
                lock (_freeNodesMonitor)
                {
                    /* while no nodes for my problem - wait for pulse */
                    while (_freeNodes.Count((e) => e.problemTypes.Contains(pname)) == 0)
                    {
                        Monitor.Wait(_freeNodesMonitor);
                    }
                    
                    /* take all free nodes and send a batch to solve */
                    var cns = _freeNodes.Where((cn) => cn.problemTypes.Contains(pname)).ToArray();
                    foreach (var cn in cns)
                    {
                        /* mark cn as busy */
                        cn.isBusy = true;
                        _freeNodes.Remove(cn);

                        /* send to solve */
                        var cnChannel = _channels[cn.id] as IComputationalNode;
                        var cnProblem = divideResponse.PartialProblems1[nextPartialIndex];
                        cnChannel.Solve(new PartialProblems()
                            {
                                Id = (ulong) pid,
                                ProblemType = pname,
                                PartialProblems1 = new PartialProblemsPartialProblem[] 
                                { 
                                    new PartialProblemsPartialProblem()
                                    {
                                        Data = cnProblem.Data,
                                        TaskId = (ulong) tids[nextPartialIndex]
                                    }
                                }
                            });
                        
                        /* if all partials were sent - stop */
                        nextPartialIndex += 1;
                        if (nextPartialIndex == partialsCount)
                            return;
                    }
                }
            }
        }

        private void SaveResultToFile(ProblemEntry pentry)
        {
            TimeSpan calculationsTook = DateTime.Now - pentry.SubmitTime;
            DvrpSolutionUnit soln = BinarySerializer.Deserialize<DvrpSolutionUnit>(pentry.Solution);
            using (FileStream fs = new FileStream(@"H:\Windows7\Desktop\mistrzostwa.txt", FileMode.Append))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine("TRWALO: " + calculationsTook.ToString());
                sw.WriteLine("DLUGOSC DROG: " + soln.TotalLength.ToString());
                foreach (var route in soln.Routes)
                {
                    sw.WriteLine(route.ToString());
                }
                sw.WriteLine();
            }
        }
    }
}
