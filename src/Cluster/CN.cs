﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cluster.ContractTypes;
using Cluster.Contracts;
using TaskSolvers.DVRP;
using UCCTaskSolver;
using System.Diagnostics;
using System.Threading;
using System.ServiceModel;

namespace Cluster
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public class CN : IComputationalNode
    {
        private Dictionary<string, TaskSolver> _pnameToSolverMap;

        private ChannelFactory<ICSForCN> _csFactory;
        
        private int _assignedId;            // id assigned by CS
        private int _threadCount;           // thread count (for us, always 1)
        private TimeSpan _refreshInterval;  // refresh msg interval
        private bool _isBusy;               // true if busy computing

        private Task _statusSendingTask;
        private CancellationTokenSource _statusSendingTokenSource;

        public CN()
        {
            _pnameToSolverMap = new Dictionary<string, TaskSolver>();
            _isBusy = false;
            _threadCount = 1; /* always 1, because solvers utilize all cores anyway */
        }

        // ===== Contract implementation =====
        public void Solve(PartialProblems partialProblems)
        {
            var channel = _csFactory.CreateChannel();
            int tid = (int) partialProblems.PartialProblems1[0].TaskId;
            Console.WriteLine(String.Format("Solving new problem [tid {0}]", tid));

            /* mark node as busy */
            Debug.Assert(_isBusy == false);
            _isBusy = true;

            /* get solver for this problem, if no sovler reply with null */
            TaskSolver solver;
            bool hasSolver = _pnameToSolverMap.TryGetValue(partialProblems.ProblemType, out solver);
            if (hasSolver == false)
            {
                channel.SubmitPartialSolution(new Solutions() { Id = partialProblems.Id, Solutions1 = null });
                return;
            }

            /* start solving */
            SolutionsSolution[] results = new SolutionsSolution[partialProblems.PartialProblems1.Length];
            for (int i = 0; i < partialProblems.PartialProblems1.Length; i++)
            {
                var pproblem = partialProblems.PartialProblems1[i];

                results[i] = new SolutionsSolution()
                {
                    Data = solver.Solve(pproblem.Data, TimeSpan.FromMilliseconds(partialProblems.SolvingTimeout)),
                    Type = SolutionsSolutionType.Partial,
                    TimeoutOccured = false,
                    TaskId = pproblem.TaskId,
                };
            }

            /* not busy anymore */
            _isBusy = false;

            /* submit the response */
            Console.WriteLine(String.Format("Partial problem solved [tid {0}]", tid));
            channel.SubmitPartialSolution(new Solutions()
            {
                Id = (ulong) _assignedId,
                ProblemType = partialProblems.ProblemType,
                Solutions1 = results
            });
        }

        // ===== Public functions =====
        public void AddTaskSolver(string problemName, TaskSolver solver)
        {
            _pnameToSolverMap[problemName] = solver;
        }

        public void RegisterToCS(ChannelFactory<ICSForCN> csFactory)
        {
            Thread.Sleep(1000);
            _csFactory = csFactory;
            var channel = _csFactory.CreateChannel();
            
            var response = channel.Register(new Register()
            {
                Type = RegisterType.ComputationalNode,
                ParallelThreads = 1,
                SolvableProblems = _pnameToSolverMap.Keys.ToArray()
            });

            _assignedId = (int) response.Id;
            _refreshInterval = TimeSpan.FromMilliseconds(response.Timeout);

            Console.WriteLine("[Registration complete] Assigned id: " + _assignedId.ToString());
            BeginSendingStatus();
        }

        public void Shutdown()
        {
            _statusSendingTokenSource.Cancel();
            _statusSendingTask.Wait();
        }

        // ===== Private utility functions =====
        private void BeginSendingStatus()
        {
            _statusSendingTokenSource = new CancellationTokenSource();
            var ct = _statusSendingTokenSource.Token;
            _statusSendingTask = Task.Run(() =>
                {
                    var channel = _csFactory.CreateChannel();
                    var msg = new Status();
                    msg.Id = (ulong)_assignedId;

                    while (ct.IsCancellationRequested == false)
                    {
                        Thread.Sleep(_refreshInterval);
                        Console.WriteLine("[Refreshing status]");

                        msg.Threads = GetThreadStatus();
                        channel.RefreshStatus(msg);
                    }
                });
        }

        private StatusThread[] GetThreadStatus()
        {
            var result = new StatusThread[_threadCount];
            for (int tid = 0; tid < _threadCount; tid++)
            {
                result[tid] = new StatusThread()
                {
                    State = _isBusy ? StatusThreadState.Busy : StatusThreadState.Idle
                };
            }
            return result;
        }
    }
}
