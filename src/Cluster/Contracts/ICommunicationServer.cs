﻿using Cluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Cluster.Contracts
{
    public interface ICommunicationServer : ICSForTM, ICSForCN, ICSForClient
    {
    }

    [ServiceContract(CallbackContract=typeof(ITaskManager))]
    public interface ICSForTM : ICSForRegisteringComponent
    {
        /* no operations here , but it's needed because....
         * 
         * this contract allows CS to GetCallbackChannel<ITaskManager>(), which then
         * can be used for CS to issue TaskManager operations whenever needed
         */
    }

    [ServiceContract(CallbackContract=typeof(IComputationalNode))]
    public interface ICSForCN : ICSForRegisteringComponent
    {
        /// <summary>
        /// Submits partial solution to CS.
        /// </summary>
        /// <param name="msg">Msg with pid, pname and task ('partial') solution (with TMid)</param>
        /// <remarks>
        /// Task (partial) solution is stored in Solutions[0] member, together with TaskManager ID which partiated the problem.
        /// </remarks>
        [OperationContract(IsOneWay=true)]
        void SubmitPartialSolution(Solutions msg);
    }

    [ServiceContract]
    public interface ICSForClient
    {
        /// <summary>
        /// Retrieve solution for a problem (pid is specified in the request msg)
        /// </summary>
        /// <param name="request">Request msg with problem id (pid)</param>
        /// <returns>Solutions containing solution to a problem in Solutions1[0] element (null if not yet complete)</returns>
        [OperationContract]
        Solutions RequestSolution(SolutionRequest solnRequest);

        /// <summary>
        /// Submits new problem to the cluster.
        /// </summary>
        /// <param name="solveRequest">Msg with problemName, problemData and possible timeout limit</param>
        /// <returns>Response with problemId assigned [pid]</returns>
        [OperationContract]
        SolveRequestResponse SubmitNewProblem(SolveRequest solveRequest);
    }

    [ServiceContract]
    public interface ICSForRegisteringComponent
    {

        /// <summary>
        /// Registers component to the Cluster.
        /// </summary>
        /// <returns>Register response with assigned id, refresh interval for statuses, backup server list</returns>
        [OperationContract]
        RegisterResponse Register(Register registerMsg);

        /// <summary>
        /// Updates component status 
        /// </summary>
        /// <param name="statusMsg">Status msg with component's ID and possibly business status (if CN)</param>
        /// <returns>Response with list of backup servers</returns>
        [OperationContract]
        NoOperation RefreshStatus(Status statusMsg);
    }
}
