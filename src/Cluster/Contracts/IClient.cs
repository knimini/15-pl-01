﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Cluster.Contracts
{
    [ServiceContract]
    public interface IClient
    {
        /* no operations here, as for now CS does not need to call any client 
         * functions in a free manner. 
         * 
         * This class exists merely for completeness and integrity of the entire solution
         */
    }
}
