﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Cluster.ContractTypes;

namespace Cluster.Contracts
{
    [ServiceContract]
    public interface IComputationalNode
    {
        /// <summary>
        /// Submits new partial problems for CN to solve
        /// </summary>
        /// <param name="partialProblems">Msg containing partial problems, problem id, problem name</param>
        /// <remarks>
        /// Tasks (partial problems) are stored in the PartialProblems1 array, where each partial problem contians 
        /// 'data' (byte array) and 'tid' (id of task manager who did partioning)
        /// </remarks>
        [OperationContract(IsOneWay=true)]
        void Solve(PartialProblems partialProblems);
    }
}
