﻿using Cluster.ContractTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Cluster.Contracts
{
    [ServiceContract]
    public interface ITaskManager
    {
        /// <summary>
        /// Issues a merging operation on task (partial) solutions
        /// </summary>
        /// <param name="msg">Msg with task (partial) solutions in the array</param>
        /// <returns>Msg with merged solution in Solutions[0]</returns>
        [OperationContract]
        Solutions MergeTasksSolutions(Solutions msg);

        /// <summary>
        /// Issues a dividing operation on a problem
        /// </summary>
        /// <param name="msg">Msg with pname, pid, pdata and # of partial problems to create</param>
        /// <returns>Divided problems (in PartialProblems1[])</returns>
        [OperationContract]
        PartialProblems DivideProblem(DivideProblem msg);
    }
}
