﻿using Cluster.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.DVRP;
using TaskSolvers.Utilities;
using Cluster.ContractTypes;

namespace ClientApp
{
    class ClientProgram
    {
        static readonly string EndpointName = "csEndpoint_v2_client";

        static readonly string OperationsDesc = "Available operations are:\n" +
            "\t'1' - to submit new dvrp problem to solve\n" +
            "\t'2' - to request solution from cluster\n" +
            "\t'1' or 'exit' - to launch a rocket to Mars";

        static ICSForClient _cs;
        static DvrpDeserializer _dvrpDeserializer = new DvrpDeserializer();

        static void Main(string[] args)
        {
            Console.WriteLine("============ SIR CLIENT V2.0 (build 1M) ==============");

            /* connect to CS */
            try
            {
                var dcf = new ChannelFactory<ICSForClient>(EndpointName);
                _cs = dcf.CreateChannel();

                Console.WriteLine("[Successfully connected to cluster!]");
                Console.WriteLine(OperationsDesc);
            }
            catch (Exception e)
            {
                Console.Write(
                    "[Failed to connect to cluster!]\n\n" +
                    e.Message + "\n\n" +
                    "Check configuration file and try again (press any key to close)");
                Console.ReadLine();
                return;
            }

            /* user input loop */
            string input; bool closeRequested = false;
            while (closeRequested == false)
            {
                Console.WriteLine("Choose next operation to perform ('1', '2', or '3'/'exit')");
                input = Console.ReadLine();
                switch (input.ToLower())
                {
                    case "1":
                        Input_SubmitNewDvrpProblem();
                        break;
                    case "2":
                        Input_RequestSolution();
                        break;
                    case "3":
                    case "exit":
                        closeRequested = true;
                        break;
                    default:
                        Console.WriteLine("Unknown operation requested");
                        break;
                }
            }
            
        }

        /* =============== INPUT HANDLING FUNCTIONS =============== */
        private static void Input_SubmitNewDvrpProblem()
        {
            Console.WriteLine("Specify problem's filename or 'cancel' to start another operation");

            /* get dvrp problem */
            DvrpProblem problem = null;
            while (problem == null)
            {
                string input = Console.ReadLine();
                input = String.Concat(@"H:\Windows7\Desktop\" + input);
                
                /* cancelled operation -> return */
                if (input.Equals("cancel", StringComparison.CurrentCultureIgnoreCase))
                    return;
                
                /* file doesn't exist -> report and prompt again */
                if (File.Exists(input) == false)
                {
                    Console.WriteLine("Specified filepath is not valid [no file exists]");
                    Console.WriteLine("Please enter the filepath again");
                    continue;
                }
                
                problem = GetDvrpProblemFromFile(input);
                /* couldn't deserialize => report and prompt again */
                if (problem == null)
                {
                    Console.WriteLine("Specified file does not contain proper dvrp problem");
                    Console.WriteLine("Please enter the another file or 'cancel' the operation");
                    continue;
                }
            }

            /* send problem to cluster */
            try
            {
                var response = _cs.SubmitNewProblem(new SolveRequest()
                {
                    Data = BinarySerializer.Serialize(problem),
                    ProblemType = DvrpBruteSolver.ProblemName
                });
                Console.WriteLine("Problem has been submitted! Your problem id is " + response.Id.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("A problem occured while submitting it to the cluster, we're genuinely sorry!\n");
                Console.WriteLine(e.Message);
            }
            return;
        }

        private static void Input_RequestSolution()
        {
            /* get problem id from the client */
            ulong pid;
            string input;
            Console.WriteLine("Please enter your problem id");
            input = Console.ReadLine();

            bool success = UInt64.TryParse(input, out pid);
            if (success == false)
            {
                Console.WriteLine("What you wrote is not a valid problem id =(");
                return;
            }
            
            /* request solution from the cluster */
            Solutions response;
            try
            {
                response = _cs.RequestSolution(new SolutionRequest() { Id = pid });
            }
            catch (Exception e)
            {
                Console.WriteLine("A problem occured while submitting it to the cluster, we're genuinely sorry!\n");
                Console.WriteLine(e.Message);
                return;
            }

            /* display the result */
            if (response.Solutions1 == null)
                Console.WriteLine("It appears that your problem is not yet ready sir.");
            else
            {
                DvrpSolutionUnit soln = BinarySerializer.Deserialize<DvrpSolutionUnit>(response.Solutions1[0].Data);
                int usedRoutes = soln.Routes.Count((r) => r.Length > 0.0);

                Console.WriteLine(
                    "SOLUTION LENGTH : " + soln.TotalLength.ToString() + "\n" +
                    "VEHICLES USED : " + usedRoutes.ToString() + " / " + soln.Routes.Count.ToString());
                for (int i = 0; i < usedRoutes; i++)
	            {
                    Console.WriteLine(soln.Routes[i].ToString());
	            }
            }
        }

        /* ============= PRIVATE UTILITY FUNCTIONS ================== */
        private static DvrpProblem GetDvrpProblemFromFile(string filename)
        {
            DvrpProblem result;
            try
            {
                result = _dvrpDeserializer.DeserializeProblem(File.ReadAllText(filename));
            }
            catch (Exception e)
            {
                result = null;
            }
            return result;
        }
    }
}
