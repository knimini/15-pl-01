﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.DVRP;

namespace IO2Tests
{
    public static class Mother
    {
        /* most important benchmarks */
        public static readonly string OurSample1Name = "IO2Tests.TaskSolvers.DvrpSamples.ourDvrp1.vrp";
        public static readonly string OurSample2Name = "IO2Tests.TaskSolvers.DvrpSamples.ourDvrp2.vrp";
        public static readonly string RafalkoTestName = "IO2Tests.TaskSolvers.DvrpSamples.io2_8_plain_a_D.vrp";
        
        /* okulewicz samples */
        public static readonly string Okul2TestName = "IO2Tests.TaskSolvers.DvrpSamples.okul12D.vrp";
        public static readonly string Okul3TestName = "IO2Tests.TaskSolvers.DvrpSamples.okul13D.vrp";
        public static readonly string Okul4TestName = "IO2Tests.TaskSolvers.DvrpSamples.okul14D.vrp";
        public static readonly string Okul5TestName = "IO2Tests.TaskSolvers.DvrpSamples.okul15D.vrp";
        public static readonly string Okul6TestName = "IO2Tests.TaskSolvers.DvrpSamples.okul16D.vrp";
        public static readonly string Okul7TestName = "IO2Tests.TaskSolvers.DvrpSamples.okul17D.vrp";

        public static DvrpDeserializer deserializer = new DvrpDeserializer();

        public static DvrpProblem CreateProblem(string resourceName)
        {
            return deserializer.DeserializeProblem(ReadResourceString(resourceName));
        }

        public static string ReadResourceString(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static void InitializeRoute(Route route, int startDepot, int endDepot, IEnumerable<int> intermediateNodes)
        {
            route.InsertFirstLap(startDepot, endDepot);
            foreach (var node in intermediateNodes)
            {
                route.InsertNode(route.Last, node);
            }
        }
    }
}
