﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;
using TaskSolvers.Utilities.Combinatorics.Distributions;

namespace IO2Tests.TaskSolvers
{
    [TestClass]
    public class DistributorsTests
    {
        /* 
         * Sadly, the explicit formula for count is beyond my mathematical skills...
         * It's like integer-partitions with constraint on component count ... 
         */
        [TestMethod]
        [TestCategory("Enumerators")]
        public void CEqualIEqualDistributor_has_correct_distributions()
        {
            int expectedCount = 10;
            int actualCount = 0;
            var distributor = ItemDistributorFactory.CreateDistributor(8, 3, false, false);
            foreach (var distr in distributor)
            {
                actualCount += 1;
            }

            Assert.AreEqual(expectedCount, actualCount);


            // 10 na 10 = 42 podziały
            distributor = ItemDistributorFactory.CreateDistributor(10, 10, false, false);
            actualCount = 0;
            foreach (var distr in distributor)
            {
                actualCount += 1;
                Assert.AreEqual(10, distr.Sum());
            }
            Assert.AreEqual(42, actualCount);
        }

        [TestMethod]
        [TestCategory("Enumerators")]
        public void CUniqueIEqualDistributor_count_is_correct()
        {
            // INITIALIZE
            int expectedCount2to4 = 10;
            int expectedCount14to4 = ((15) * (16) * (17)) / 6 /*(4-1)!*/;

            int actualCount2to4 = 0;
            int actualCount14to4 = 0;
            var distributions1 = ItemDistributorFactory.CreateDistributor(2, 4, false, true);
            var distributions2 = ItemDistributorFactory.CreateDistributor(14, 4, false, true);

            // EXECUTE
            actualCount2to4 = distributions1.Count();
            actualCount14to4 = distributions2.Count();

            // ASSERT
            Assert.AreEqual(expectedCount2to4, actualCount2to4);
            Assert.AreEqual(expectedCount14to4, actualCount14to4);
        }

        [TestMethod]
        [TestCategory("Enumerators")]
        public void CUniqueIEqualDistributor_are_unique_and_valid()
        {
            // INITIALIZE
            int itemCount = 10; int containerCount = 3;
            var distributions = ItemDistributorFactory.CreateDistributor(itemCount, containerCount, false, true);
            var collected = new List<int[]>();

            // EXECUTE & ASSERT
            foreach (var distr in distributions)
            {
                Assert.IsNull(
                    collected.FirstOrDefault((item) => MoreLinq.ArrayEquals<int>(distr as int[], item)));
                collected.Add(distr as int[]);

                Assert.AreEqual(itemCount, distr.Sum());
            }
        }

        private int Factorial(int n)
        {
            int result = 1;
            for (int i = 0; i <= n; i++)
                result *= i;
            return result;
        }

        private int NewtonSymbol(int k, int n)
        {
            long nominator = 1;
            long denominator = 1;

            for (int i = n - k + 1; i <= n; i++)
                nominator *= i;
            for (int i = 1; i <= k; i++)
                denominator *= i;

            return (int)(nominator / denominator);
        }
    }
}
