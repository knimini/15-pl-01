﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers;

namespace IO2Tests.TaskSolvers
{
    [TestClass]
    public class SummationSolverTests
    {
        [TestMethod]
        [TestCategory("Summation Solver")]
        public void SummationSolver_division_when_there_are_more_threads_than_elements()
        {
            // initialize
            int elemCount = 10, threadCount = 15, elemValue = 500;
            int[] intData = new int[elemCount];
            for (int i = 0; i < elemCount; i++)
			{
			    intData[i] = elemValue;
			}

            byte[] data = new byte[Buffer.ByteLength(intData)];
            Buffer.BlockCopy(intData, 0, data, 0, data.Length);
            
            // execute
            SummationSolver solver = new SummationSolver(data);
            byte[][] actual = solver.DivideProblem(threadCount);
            
            // assert
            int expectedSubProblems = elemCount;
            int expectedSubproblemLength = sizeof(int);

            Assert.AreEqual(expectedSubProblems, actual.Length);
            foreach (var subproblem in actual)
            {
                Assert.AreEqual(expectedSubproblemLength, subproblem.Length);
                Assert.AreEqual(elemValue, BitConverter.ToInt32(subproblem, 0));
            }
        }

        [TestMethod]
        [TestCategory("Summation Solver")]
        public void SummationSolver_division_and_solving_of_large_array()
        {
            // initialize
            int elemCount = 800, threadCount = 15, elemValue = 600;
            int[] intData = new int[elemCount];
            for (int i = 0; i < elemCount; i++)
            {
                intData[i] = elemValue;
            }

            byte[] data = new byte[Buffer.ByteLength(intData)];
            Buffer.BlockCopy(intData, 0, data, 0, data.Length);

            // execute
            SummationSolver solver = new SummationSolver(data);
            byte[][] subproblems = solver.DivideProblem(threadCount);
            byte[][] solutions = new byte[subproblems.Length][];
            for (int i = 0; i < subproblems.Length; i++)
            {
                solutions[i] = solver.Solve(subproblems[i], TimeSpan.MaxValue);
            }
            int actual = BitConverter.ToInt32(solver.MergeSolution(solutions), 0);

            // assert
            int expected = elemValue * elemCount;
            Assert.AreEqual(expected, actual);
        }
    }
}
