﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.DVRP;
using TaskSolvers.Utilities;

namespace IO2Tests.TaskSolvers
{
    [TestClass]
    public class DvrpDeserializationTests
    {
        // ukul sample 12a test
        [TestMethod]
        [TestCategory("Dvrp-Serialization")]
        public void OkulewiczSample1_deserialized_properly()
        {
            // initialize
            string vrpText = Mother.ReadResourceString("IO2Tests.TaskSolvers.DvrpSamples.okul12D.vrp");

            // execute
            var deserializer = new DvrpDeserializer();
            var actual = deserializer.DeserializeProblem(vrpText);

            // assert
            int expectedDepotCount = 1;
            int expectedCapacity = 100;
            int expectedVehicleCount = 12;
            int expectedStopPointCount = 13;
            int expectedDepot0Since = 0;
            int expectedDepot0Until = 640;
            int expectedDurations = 20;

            Assert.AreEqual(expectedCapacity, actual.VehicleCapacity);
            Assert.AreEqual(expectedDepotCount, actual.DepotIds.Count());
            Assert.AreEqual(expectedVehicleCount, actual.VehicleCount);
            Assert.AreEqual(expectedStopPointCount, actual.StopPoints.Length);
            Assert.AreEqual(expectedDepot0Since, actual.StopPoints[0].availSince);
            Assert.AreEqual(expectedDepot0Until, actual.StopPoints[0].availUntil);
            Assert.IsTrue(actual.StopPoints.Skip(1).All((sStopPoint) => sStopPoint.unloadDuration == expectedDurations));
        }

        [TestMethod]
        [TestCategory("Dvrp-Serialization")]
        public void OkulewiczSample4_deserialized_properly()
        {
            // initialize
            string vrpText = Mother.ReadResourceString("IO2Tests.TaskSolvers.DvrpSamples.okul14D.vrp");

            // execute
            var deserializer = new DvrpDeserializer();
            var actual = deserializer.DeserializeProblem(vrpText);

            // assert
            int expectedDepotCount = 1;
            int expectedCapacity = 100;
            int expectedVehicleCount = 14;
            int expectedStopPointCount = 15;
            int expectedDurations = 20;
            int[] expectedTimeAvailSince = new int[] { 0, 199, 0, 0, 0, 82, 0, 0, 0, 68, 209, 0, 113, 0, 0 };
            int expectedDepot0AvailUntil = 680;

            Assert.AreEqual(expectedCapacity, actual.VehicleCapacity);
            Assert.AreEqual(expectedDepotCount, actual.DepotIds.Count());
            Assert.AreEqual(expectedVehicleCount, actual.VehicleCount);
            Assert.AreEqual(expectedStopPointCount, actual.StopPoints.Length);
            Assert.IsTrue(actual.StopPoints.Skip(1).All((sStopPoint) => sStopPoint.unloadDuration == expectedDurations));
            CollectionAssert.AreEqual(
                expectedTimeAvailSince,
                actual.StopPoints.Select((sStopPoint) => sStopPoint.availSince).ToArray());
            Assert.AreEqual(expectedDepot0AvailUntil, actual.StopPoints[0].availUntil);
        }

        [TestMethod]
        [TestCategory("Dvrp-Serialization")]
        public void OkulewiczSample7_deserialized_properly()
        {
            // initialize
            string vrpText = Mother.ReadResourceString("IO2Tests.TaskSolvers.DvrpSamples.okul17D.vrp");

            // execute
            var deserializer = new DvrpDeserializer();
            var actual = deserializer.DeserializeProblem(vrpText);

            // assert
            int expectedDepotCount = 1;
            int expectedCapacity = 100;
            int expectedVehicleCount = 17;
            int expectedStopPointCount = 18;
            int expectedDurations = 20;
            int[] expectedTimeAvailSince = new int[] { 0, 299, 0, 0, 135, 23, 101, 296, 174, 0, 0, 195, 0, 193, 125, 0, 0, 0 };
            int expectedDepot0AvailUntil = 740;

            Assert.AreEqual(expectedCapacity, actual.VehicleCapacity);
            Assert.AreEqual(expectedDepotCount, actual.DepotIds.Count());
            Assert.AreEqual(expectedVehicleCount, actual.VehicleCount);
            Assert.AreEqual(expectedStopPointCount, actual.StopPoints.Length);
            Assert.IsTrue(actual.StopPoints.Skip(1).All((sStopPoint) => sStopPoint.unloadDuration == expectedDurations));
            CollectionAssert.AreEqual(
                expectedTimeAvailSince,
                actual.StopPoints.Select((sStopPoint) => sStopPoint.availSince).ToArray());
            Assert.AreEqual(expectedDepot0AvailUntil, actual.StopPoints[0].availUntil);
        }

        [TestMethod]
        [TestCategory("Dvrp-Serialization")]
        public void Binary_serialization_of_a_problem()
        {
            var problem = Mother.CreateProblem(Mother.OurSample2Name);

            byte[] bytes = BinarySerializer.Serialize(problem);
            var deserializedProblem = BinarySerializer.Deserialize<DvrpProblem>(bytes);

            Assert.AreEqual(problem, deserializedProblem);
        }
    }
}
