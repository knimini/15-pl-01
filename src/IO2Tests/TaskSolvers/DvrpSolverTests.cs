﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.DVRP;
using TaskSolvers.Utilities;
using UCCTaskSolver;

namespace IO2Tests.TaskSolvers
{
    [TestClass]
    public class DvrpSolverTests
    {
        #region Genetic solver

        [TestMethod]
        [TestCategory("Dvrp Solver - Genetic")]
        public void Sample1_with_capacity_only_gives_almost_perfect_result()
        {
            var problem = Mother.CreateProblem(Mother.OurSample1Name);
            problem.TimeConstraints = false;

            double bestSolution = (8 + 2 * Math.Sqrt(2)) * 2;
            double tolerance = 0.05 * bestSolution;
            int threadCount = 1;

            RunBenchmarkOn(typeof(DvrpBruteSolver), problem, bestSolution, threadCount, tolerance);
        }

        [TestMethod]
        [TestCategory("Dvrp Solver - Genetic")]
        public void Sample2_with_capacity_only_gives_almost_pefect_result()
        {
            var problem = Mother.CreateProblem(Mother.OurSample2Name);
            problem.TimeConstraints = false;

            double bestSolution = 20 * 2 + 2 * Math.Sqrt(2);
            double tolerance = 0.05 * bestSolution;
            int threadCount = 1;

            RunBenchmarkOn(typeof(DvrpBruteSolver), problem, bestSolution, threadCount, tolerance);
        }

        [TestMethod]
        [TestCategory("Dvrp Solver - Genetic")]
        public void Sample2_gives_almost_pefect_result()
        {
            // INITIALIZE
            // create problem & modify a problem a bit, so that best solution is obvious
            var problem = Mother.CreateProblem(Mother.OurSample2Name);
            problem.VehicleCapacity = 110;
            problem.StopPoints[8].availUntil = 1000;
            problem.StopPoints[12].availUntil = 1000;
            
            /* route 1 start = 8, route 1 end = 12; */
            var route1 = new int[] { 14, 13, 7, 0, 1, 2, 3, 4, 5, 6 };
            /* route 2 end = 12, route 2 end = 8 */
            var route2 = new int[] { 19, 18, 11, 10, 17, 16, 15, 9 };
            double edgeLength = 2.0;
            double nodeUnloadTime = 20.0;

            for (int i = 0; i < route1.Length; i++)
                problem.StopPoints[route1[i]].availSince = (int)Math.Floor((i + 1) * (edgeLength + nodeUnloadTime));
            for (int i = 0; i < route2.Length; i++)
                problem.StopPoints[route2[i]].availSince = (int)Math.Floor((i + 1) * (edgeLength + nodeUnloadTime));

            // set expectation
            double bestSolution = ((route1.Length + 1) + (route2.Length + 1)) * edgeLength;
            double tolerance = 0.05 * bestSolution;
            int threadCount = 4;

            // EXECUTE & ASSERT
            RunBenchmarkOn(typeof(DvrpBruteSolver), problem, bestSolution, threadCount, tolerance);
        }

        [TestMethod]
        [TestCategory("Dvrp Solver - Genetic")]
        public void Test_on_doktora_Rafalko_tez_smiga()
        {
            var rafalkoProblem = Mother.CreateProblem(Mother.RafalkoTestName);
            double bestSolution = 680.09;
            double tolerance = 0.05 * bestSolution;
            int threadCount = 4;

            RunBenchmarkOn(typeof(DvrpBruteSolver), rafalkoProblem, bestSolution, threadCount, tolerance);
        }

        #endregion // Genetic Solver

        #region Brute Solver

        [TestMethod]
        [TestCategory("Dvrp Solver - Brute")]
        public void Sample1_gives_perfect_result()
        {
            var problem = Mother.CreateProblem(Mother.OurSample1Name);

            double bestSolution = (8 + 2 * Math.Sqrt(2)) * 2;
            double tolerance = TolerantComparer.DEFAULT_TOLERANCE;
            int threadCount = 1;

            RunBenchmarkOn(typeof(DvrpBruteSolver), problem, bestSolution, threadCount, tolerance);
        }

        [TestMethod]
        [TestCategory("Dvrp Solver - Brute")]
        public void Sample1_multi_depot_gives_perfect_result()
        {
            var problem = Mother.CreateProblem(Mother.OurSample1Name);

            problem.DepotIds = new int[] { 0, 2, 4, 6, 8 };
            problem.VehicleCount = 4;
            problem.ClientCount = 9 - 5;

            problem.StopPoints[0] = problem.StopPoints[2] = problem.StopPoints[4] =
            problem.StopPoints[6] = problem.StopPoints[8] = 
                new StopPoint()
                {
                    availSince = 0,
                    availUntil = 300,
                    isDepot = true,
                    demand = 0,
                    unloadDuration = 0
                };

            double bestSolution = 3 * Math.Sqrt(8) + 2 * 2;
            double tolerance = TolerantComparer.DEFAULT_TOLERANCE;
            int threadCount = 1;

            RunBenchmarkOn(typeof(DvrpBruteSolver), problem, bestSolution, threadCount, tolerance);
        }


        [TestMethod]
        [TestCategory("Dvrp Solver - Brute")]
        public void Rafalko_brutally_solved_gives_perfect_result()
        {
            var problem = Mother.CreateProblem(Mother.RafalkoTestName);
            int threadCount = 1;

            double bestSolution = 680.09;
            double tolerance = TolerantComparer.DEFAULT_TOLERANCE;

            RunBenchmarkOn(typeof(DvrpBruteSolver), problem, bestSolution, threadCount, tolerance);
        }


        /* ======= this can take forever ========== */
        //[TestMethod]
        //[TestCategory("Dvrp Solver - Brute")]
        //public void Many_other_benchmarks()
        //{
        //    string[] problemNames = new string[]
        //    {
        //        Mother.Okul2TestName,
        //        Mother.Okul3TestName,
        //        Mother.Okul4TestName,
        //        Mother.Okul5TestName,
        //        Mother.Okul6TestName,
        //        Mother.Okul7TestName
        //    };
        //    double[] expectedResults = new double[]
        //    {
        //        976.27, // 2
        //        1154.38,
        //        948.59,
        //        1105.72,// 5
        //        989.01,
        //        1089.36
        //    };

        //    int nodeCount = 1;
        //    double tolerance = 0.1;
        //    for (int i = 0; i < problemNames.Length; i++)
        //    {
        //        var problem = Mother.CreateProblem(problemNames[i]);
        //        RunBenchmarkOn(typeof(DvrpBruteSolver), problem, expectedResults[i], nodeCount, tolerance);
        //    }
        //}

        #endregion // Brute Solver

        private void RunBenchmarkOn(Type solverType, DvrpProblem problem, double expectedResult, int threadCount = 4, double tolerance = TolerantComparer.DEFAULT_TOLERANCE)
        {
            // INITIALIZE 
            var solver = (TaskSolver) Activator.CreateInstance(solverType, BinarySerializer.Serialize(problem));
            Assert.IsNotNull(solver);

            // EXECUTE
            var rawSolution = RunSolver(solver, problem, threadCount);
            DvrpSolutionUnit actualSolution = (solver as IDvrpSolver).DeserializeSolution(rawSolution);

            // ASSERT
            Assert.AreNotEqual(actualSolution, 0.0);
            Assert.IsTrue(TolerantComparer.AreEqual(actualSolution.TotalLength, expectedResult));
        }

        /* runs solver on a dvrpSolver and returns byte array */
        private byte[] RunSolver(TaskSolver solver, DvrpProblem problem, int threadCount)
        {
            byte[][] subproblems = solver.DivideProblem(threadCount);
            byte[][] solutions = new byte[subproblems.Length][];
            Task[] tasks = new Task[threadCount];

            for (int i = 0; i < subproblems.Length; i++)
            {
                // solutions[i] = solver.Solve(subproblems[i], TimeSpan.MaxValue);
                int index = i;
                tasks[index] = new Task(() =>
                    {
                        Stopwatch watch = new Stopwatch();
                        var nodeSolver = new DvrpBruteSolver(null);
                        
                        watch.Start();
                        solutions[index] = nodeSolver.Solve(subproblems[index], TimeSpan.MaxValue);
                        watch.Stop();

                        Debug.WriteLine("Node #" + index.ToString() + " took " + watch.Elapsed.ToString());
                    });
                tasks[index].Start();
            }
            Task.WaitAll(tasks);

            return solver.MergeSolution(solutions);
        }
    }
}
