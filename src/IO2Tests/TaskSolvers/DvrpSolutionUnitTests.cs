﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.DVRP;

namespace IO2Tests.TaskSolvers
{
    [TestClass]
    public class DvrpSolutionUnitTests
    {
        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Insertion_updates_load_on_route_arcs()
        {
            var problem = Mother.CreateProblem(Mother.OurSample1Name);
            DvrpSolutionUnit unit = new DvrpSolutionUnit(problem);

            // initialize route
            var route = unit.CreateNewRoute();
            int depot = 4;
            int[] routeA = new int[] { 3, 0, 1, 2 };

            // execute insertions
            route.InsertFirstLap(depot, depot);
            foreach (var node in routeA)
                route.InsertNode(route.Last, node);

            // assert
            int expectedLoad = 40;
            foreach (var routeArc in route)
            {
                Assert.AreEqual(expectedLoad, routeArc.Load);
                expectedLoad -= 10;
            }
        }


        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Insertion_keeps_times_consistent()
        {
            var problem = Mother.CreateProblem(Mother.OurSample1Name);
            DvrpSolutionUnit unit = new DvrpSolutionUnit(problem);

            // INITIALIZE
            // first create route
            var route = unit.CreateNewRoute();
            int depot = 4;
            int[] routeA = new int[] { 3, 0, 1, 2 };
            double[] expectedArrivals = new double[routeA.Length + 1];
            double[] expectedDelays = new double[routeA.Length + 1];

            // calculate expected values for this route
            double depart = 0;
            for (int i = 0; i < expectedArrivals.Length; i++)
            {
                int arcEnd = (i == routeA.Length) ? depot : routeA[i];
                int arcStart = (i == 0) ? depot : routeA[i - 1];

                double arcITravelTime = problem.GetTravelTime(arcStart, arcEnd);
                expectedDelays[i] = Math.Max(
                    0,
                    problem.StopPoints[arcEnd].availSince - (depart + arcITravelTime));
                expectedArrivals[i] = depart + expectedDelays[i] + arcITravelTime;

                depart = expectedArrivals[i] + problem.StopPoints[arcEnd].unloadDuration;
            }

            // EXECUTE INSERTIONS
            route.InsertFirstLap(depot, depot);
            foreach (var node in routeA)
                route.InsertNode(route.Last, node);

            // ASSERT
            var actualArc = route.First;
            for (int i = 0; i < routeA.Length + 1; i++)
            {
                Assert.AreEqual(expectedDelays[i], actualArc.DelayTime);
                Assert.AreEqual(expectedArrivals[i], actualArc.ArrivalAtDestination);
                actualArc = actualArc.Next;
            }
        }

        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Is_feasible_insertion_due_to_load()
        {
            // INITIALIZE 
            var problem = Mother.CreateProblem(Mother.OurSample1Name);
            problem.TimeConstraints = false;
            /* problem.VehicleCapacity = 40; */

            // inserting node 0 will put too much load
            problem.StopPoints[0].demand = 31;
            // inserting node 1 will be fine
            problem.StopPoints[1].demand = 20;
            // inserting node 2 will use the vehicle to it's fullest
            problem.StopPoints[2].demand = 30;
            DvrpSolutionUnit unit = new DvrpSolutionUnit(problem);

            int depot = 4;
            var someRoute = new int[] { 3 };
            var route = unit.CreateNewRoute();
            Mother.InitializeRoute(route, depot, depot, someRoute);

            // EXECUTE & ASSERT
            Assert.IsFalse(route.IsFeasibleInsertion(route.First, 0));
            Assert.IsTrue(route.IsFeasibleInsertion(route.First, 1));
            Assert.IsTrue(route.IsFeasibleInsertion(route.First, 2));
        }

        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Is_feasible_insertion_due_to_time()
        {
            // INITIALIZE
            int depot = 4;
            var someRoute1 = new int[] { 0, 1, 2, 3 };
            var problem = Mother.CreateProblem(Mother.OurSample1Name);
            problem.CapacityConstraint = false;
            problem.StopPoints[depot].availUntil = 600;

            // node 7 can be served just in time
            problem.StopPoints[7].availSince = 580 - (int)Math.Ceiling(problem.GetTravelTime(7, depot));
            problem.StopPoints[7].unloadDuration = 20;
            // node 6 is slightly too late, can never be served
            problem.StopPoints[6].availSince = 580 - (int)Math.Floor(problem.GetTravelTime(6, depot));
            problem.StopPoints[6].unloadDuration = 20;
            // node 5 takes to long to unload
            problem.StopPoints[5].unloadDuration = 100;
            problem.StopPoints[5].availSince = 499;
            // other nodes are available to insert

            // create route
            DvrpSolutionUnit unit = new DvrpSolutionUnit(problem);
            var route = unit.CreateNewRoute();
            Mother.InitializeRoute(route, depot, depot, someRoute1);

            // EXECUTE & ASSERT
            Assert.IsTrue(route.IsFeasibleInsertion(route.Last, 7));
            Assert.IsFalse(route.IsFeasibleInsertion(route.Last, 6));
            Assert.IsFalse(route.IsFeasibleInsertion(route.Last, 5));
            Assert.IsTrue(route.IsFeasibleInsertion(route.Last, 8));
        }

        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Route_arcs_in_solution_are_connected()
        {
            // INITIALIZE
            var myProblem2 = Mother.CreateProblem(Mother.OurSample2Name);

            // EXECUTE
            var unit = GeneticOperations.SpawnSolution(myProblem2);

            // ASSERT
            foreach (var route in unit.Routes)
            {
                // first arc is out of depot
                Assert.IsTrue(myProblem2.IsDepot(route[0].From));

                // middle arcs are connected
                for (int i = 0; i < route.Count - 1; i++)
                    Assert.IsTrue(route[i].To == route[i + 1].From);

                // last arc ends at a depot
                Assert.IsTrue(myProblem2.IsDepot(route[route.Count - 1].To));
            }
        }

        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Spawned_solutions_are_diverse()
        {
            // INITIALIZE
            var problem = Mother.CreateProblem(Mother.OurSample2Name);
            DvrpSolutionUnit[] population = new DvrpSolutionUnit[50];

            // EXECUTE
            for (int i = 0; i < population.Length; i++)
                population[i] = GeneticOperations.SpawnSolution(problem);

            // ASSERT
            // count solutions by length - assert there are at least 2 different
            Dictionary<double, int> lengthCountMap = new Dictionary<double, int>();
            foreach (var unit in population)
            {
                if (lengthCountMap.ContainsKey(unit.TotalLength) == false)
                    lengthCountMap.Add(unit.TotalLength, 1);
                else
                    lengthCountMap[unit.TotalLength] += 1;
            }

            Assert.IsTrue(lengthCountMap.Keys.Count > 1);
        }

        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Crossover_preserves_common_nodes()
        {
            // INITIALIZE PARENTS AND PROBLEMS
            var problem = Mother.CreateProblem(Mother.OurSample2Name);
            problem.TimeConstraints = false;
            problem.VehicleCapacity = 1000;

            int[] dadRouteA = new int[] { 0, 1, 2, 3, 4 };
            int[] dadRouteB = new int[] { 7, 13, 14, 9, 15, 16, 10, 17, 11, 18, 19 };
            int[] dadUnassigned = new int[] { 5, 6 };
            int[] momRouteA = new int[] { 0, 1, 2, 3, 4, 5, 6 };
            int[] momRouteB = new int[] { 13, 14, 15, 16, 17, 18, 19 };
            int[] momUnassigned = new int[] { 7, 9, 10, 11 };

            int[] expectedCommonNodes = new int[] { 13, 14, 15, 16, 17, 18, 19 };

            DvrpSolutionUnit mom = new DvrpSolutionUnit(problem);
            DvrpSolutionUnit dad = new DvrpSolutionUnit(problem);
            Mother.InitializeRoute(mom.CreateNewRoute(), 8, 12, momRouteA);
            Mother.InitializeRoute(mom.CreateNewRoute(), 8, 12, momRouteB);
            Mother.InitializeRoute(dad.CreateNewRoute(), 8, 12, dadRouteA);
            Mother.InitializeRoute(dad.CreateNewRoute(), 8, 12, dadRouteB);
            dad.UnvisitedNodes = new HashSet<int>(dadUnassigned);
            mom.UnvisitedNodes = new HashSet<int>(momUnassigned);

            // EXECUTE
            var child = GeneticOperations.CommonNodesCrossover(mom, dad);

            // ASSERT
            var preservedRoute = child.Routes.FirstOrDefault((r) => r.ContainsNode(expectedCommonNodes.First()));
            Assert.IsNotNull(preservedRoute);
            foreach (var commonNode in expectedCommonNodes)
                Assert.IsTrue(preservedRoute.ContainsNode(commonNode));
        }

        [TestMethod]
        [TestCategory("Insertion Heuristics")]
        public void Mutation_does_actually_mutate()
        {
            // initialize problem and sample unit
            var problem = Mother.CreateProblem(Mother.OurSample2Name);
            var unit = new DvrpSolutionUnit(problem);

            int[] route1 = new int[] { 13, 7, 0, 14, 8, 1, 15, 9, 2, 16, 3 };
            int route1Start = 8, route1End = 8;

            int[] route2 = new int[] { 17, 10, 4, 12, 18, 11, 5, 19, 6 };
            int route2Start = 12, route2End = 12;

            Mother.InitializeRoute(unit.CreateNewRoute(), route1Start, route1End, route1);
            Mother.InitializeRoute(unit.CreateNewRoute(), route2Start, route2End, route2);

            // mutate sample unit
            DvrpSolutionUnit[] mutants = new DvrpSolutionUnit[10];
            for (int i = 0; i < 10; i++)
                mutants[i] = GeneticOperations.SubsetReinsertionMutation(unit);

            // assert different lengths
            Assert.IsFalse(mutants.All((sMutant) => sMutant.TotalLength == unit.TotalLength));
        }
    }
}
