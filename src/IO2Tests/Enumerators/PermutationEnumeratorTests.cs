﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;
using System.Numerics;
using System.Diagnostics;
using TaskSolvers.Utilities.Combinatorics;

namespace IO2Tests.Enumerators
{
    [TestClass]
    public class PermutationEnumeratorTests
    {
        [TestMethod]
        [TestCategory("Enumerators")]
        public void Permutations_count_is_correct()
        {
            PermutationEnumerator enumerator10 = new PermutationEnumerator(10);
            PermutationEnumerator enumerator20 = new PermutationEnumerator(20);

            ulong expectedCount10 = 3628800; /* 10! */
            ulong expectedCount20 = 2432902008176640000; /* 20! */

            Assert.IsTrue(expectedCount10 == enumerator10.Count);
            Assert.IsTrue(expectedCount20 == enumerator20.Count);
        }

        [TestMethod]
        [TestCategory("Enumerators")]
        public void Permutations_are_unique()
        {
            HashSet<Permutation> permutations = new HashSet<Permutation>();

            var enumerator = new PermutationEnumerator(5);
            while (enumerator.MoveNext())
            {
                var permutation = enumerator.Current;
                Assert.IsTrue(permutations.Add(permutation));
            }
        }

        [TestMethod]
        [TestCategory("Enumerators")]
        public void Combinations_count_is_correct()
        {
            int totalCount = 20; int selectCount = 6;
            int expected = 38760;
            // int actual = 0;

            var combinations = new Combinations<int>(Enumerable.Range(1, totalCount).ToArray(), selectCount);
            Assert.AreEqual(expected, combinations.Count);
        }

        [TestMethod]
        [TestCategory("Enumerators")]
        public void Combinations_are_unique()
        {
            int totalCount = 10; int selectCount = 2;
            int expected = 45; int actualCount = 0;

            var set = new HashSet<Permutation>();
            // int actual = 0;

            var combinations = new Combinations<int>(Enumerable.Range(1, totalCount).ToArray(), selectCount);
            foreach (var combination in combinations)
            {
                bool added = set.Add(new Permutation(combination.ToArray()));
                Assert.IsTrue(added);
                actualCount += 1;
            }

            Assert.AreEqual(expected, actualCount);
        }
    }
}
