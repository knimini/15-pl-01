﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Cluster;
using Cluster.Contracts;
using System.ServiceModel.Description;

namespace CommunicationServerNodeProgram
{
    class CSProgram
    {
        static void PrintServiceInfo(ServiceHost host)
        {
            Console.WriteLine("{0} is up and running with these endpoints:", host.Description.ServiceType);
            foreach (ServiceEndpoint se in host.Description.Endpoints)
                Console.WriteLine(se.Address);
        }

        static void Main(string[] args)
        {
            //var appSettings = System.Configuration.ConfigurationManager.AppSettings;
            
            //int ind = Array.IndexOf(args, "-port");
            //string port = (ind >= 0 && args.Length > ind+1? args[ind + 1] : appSettings.Get("port"));

            //bool backup = args.Contains("backup");
            //backup |= appSettings.Get("backup") == "true";


            //CommunicationServerNode node;

            //if (backup)
            //    node = new CommunicationServerNode("8081", backup);
            //else
            //    node = new CommunicationServerNode("8080", backup);
            //node.Start();

            Console.WriteLine("============ COMMUNICATION SERVER V2.0 (build 78420) ==============");
            var cs = new CS(TimeSpan.FromSeconds(2));
            ServiceHost host = new ServiceHost(cs, new Uri[] { new Uri("net.tcp://localhost:8080") });  
            try
            {
                host.Open();
                PrintServiceInfo(host);
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("CRITICAL ERROR : " + e.Message);
            }
            finally
            {
                host.Close();
            }
        }
    }
}
