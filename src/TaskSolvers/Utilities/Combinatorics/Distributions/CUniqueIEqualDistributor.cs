﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities.Combinatorics.Distributions
{
    public class CUniqueIEqualDistributor : IItemDistributor
    {
        int _itemCount;
        int _containerCount;

        public CUniqueIEqualDistributor(int itemCount, int containerCount)
        {
            _itemCount = itemCount;
            _containerCount = containerCount;
        }

        public IEnumerator<int[]> GetEnumerator()
        {
            if (_containerCount == 1)
                return new SingleContainerEnumerator(_itemCount);
            else
                return new MultiContainerEnumerator(_itemCount, _containerCount);
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return GetEnumerator(); }

        #region Inner Enumerator classes

        public class SingleContainerEnumerator : IEnumerator<int[]>
        {
            int _itemCount;
            bool _reseted;
 
            internal SingleContainerEnumerator(int itemCount)
            {
                _reseted = true;
                _itemCount = itemCount;
            }

            public int[] Current { get { return new int[] { _itemCount }; } }
            object System.Collections.IEnumerator.Current { get { return new int[] { _itemCount }; } }
            public void Reset() { _reseted = true; }
            public void Dispose() { }         

            public bool MoveNext()
            {
                bool result = _reseted;
                _reseted = !_reseted;
                return result;
            }
        }

        public class MultiContainerEnumerator : IEnumerator<int[]>
        {
            /* What is a STICK_POSITION? That's how we call boundary markers position.
             * 
             * Every distribution can be reached in the following way:
             * - put items in one long line (they are undiscernible)
             * - put 2 sticks that mark containers' boundaries
             * 
             * Assuming that O, I represent item and stick respectively then:
             * O O I O O I O  - means distribution 2-2-1
             */
            int[] _stickPos;
            int _itemCount;

            internal MultiContainerEnumerator(int itemCount, int containerCount)
            {
                _stickPos = new int[containerCount - 1];
                _itemCount = itemCount;
                Reset();
            }

            public int[] Current { get { return CreateDistributionFromSticks(); } }
            object System.Collections.IEnumerator.Current { get { return Current; } }
            public void Dispose() { }

            public bool MoveNext()
            {
                for (int i = 0; i < _stickPos.Length - 1; i++)
                {
                    if (_stickPos[i] < _stickPos[i + 1])
                    {
                        _stickPos[i] += 1;
                        return true;
                    }
                    else
                    {
                        _stickPos[i] = 0;
                    }
                }

                if (_stickPos.Last() < _itemCount)
                {
                    _stickPos[_stickPos.Length - 1] += 1;
                    return true;
                }

                return false;
            }

            public void Reset()
            {
                Array.Clear(_stickPos, 0, _stickPos.Length);
                _stickPos[_stickPos.Length - 1] = -1;
            }

            private int[] CreateDistributionFromSticks()
            {
                int[] counts = new int[_stickPos.Length + 1];
            
                counts[0] = _stickPos[0];
                for (int i = 1; i < counts.Length - 1; i++)
                    counts[i] = _stickPos[i] - _stickPos[i - 1];
                counts[counts.Length - 1] = _itemCount - _stickPos.Last();

                return counts;
            }
        }

        #endregion // Inner Enumerator classes
    }
}
