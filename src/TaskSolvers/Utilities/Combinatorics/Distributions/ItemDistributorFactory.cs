﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities.Combinatorics.Distributions
{
    public static class ItemDistributorFactory
    {
        public static IItemDistributor CreateDistributor(int itemCount, int containerCount, bool itemsDistinguishable, bool containersDistinguishable)
        {
            if (itemsDistinguishable == false)
            {
                if (containersDistinguishable == false)
                    return new CEqualIEqualDistributor(itemCount, containerCount);
                else
                    return new CUniqueIEqualDistributor(itemCount, containerCount);
            }
            else
                throw new NotImplementedException();
        }
    }
}
