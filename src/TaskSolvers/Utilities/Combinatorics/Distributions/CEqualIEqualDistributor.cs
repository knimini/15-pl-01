﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities.Combinatorics.Distributions
{
    public class CEqualIEqualDistributor : IItemDistributor
    {
        int _itemCount, _containerCount;

        public CEqualIEqualDistributor(int itemCount, int containerCount)
        {
            _itemCount = itemCount;
            _containerCount = containerCount;
        }

        public IEnumerator<int[]> GetEnumerator()
        {
            if (_containerCount == 1)
                return new SingleContainerEnumerator(_itemCount);
            else
                return new MultiContainerEnumerator(_itemCount, _containerCount);
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return GetEnumerator(); }

        #region Inner Enumerator Class

        public class SingleContainerEnumerator : IEnumerator<int[]>
        {
            int _itemCount;
            bool _reseted;

            internal SingleContainerEnumerator(int itemCount)
            {
                _itemCount = itemCount;
                _reseted = true;
            }

            public int[] Current { get { return new int[] { _itemCount }; } }
            object System.Collections.IEnumerator.Current { get { return Current; } }
            public void Dispose() { }          

            public bool MoveNext()
            {
                bool result = _reseted;
                _reseted = !_reseted;
                return result;
            }
            public void Reset() { _reseted = true; }
        }

        public class MultiContainerEnumerator : IEnumerator<int[]>
        {
            int _itemCount;
            int[] _counts;
            int[] _availItems;

            int ContainerCount { get { return _counts.Length; } }

            internal MultiContainerEnumerator(int itemCount, int containerCount)
            {
                _counts = new int[containerCount];
                _availItems = new int[containerCount];
                _itemCount = itemCount;
                Reset();
            }

            public int[] Current
            {
                get 
                {
                    int[] result = new int[_counts.Length];
                    Array.Copy(_counts, result, _counts.Length);
                    return result;
                }
            }
            object System.Collections.IEnumerator.Current { get { return Current; } }
            public void Dispose() { }

            public bool MoveNext()
            {
                if (IncrementPosition1() == true)
                    return true;

                int pos = 2;
                while (pos < ContainerCount)
                {
                    if (IncrementPosition(pos))
                        return true;
                    else
                        pos += 1;
                }
                return false;
            }

            public void Reset()
            {
                Array.Clear(_counts, 0, _counts.Length);
                for (int i = 0; i < _availItems.Length; i++)
                    _availItems[i] = _itemCount;

                _counts[1] = -1;
                _counts[0] = _itemCount + 1;
            }

            private bool IncrementPosition1()
            {
                if (_counts[1] + 1 <= _counts[0] - 1)
                {
                    _counts[1] += 1;
                    _counts[0] -= 1;
                    return true;
                }
                else
                    return false;
            }

            private bool IncrementPosition(int pos)
            {
                Debug.Assert(pos >= 2);
                _counts[pos] += 1;

                int newVal = _counts[pos];
                int newAvail = _availItems[pos];
                for (int i = pos - 1; i > 0; i--)
                {
                    newAvail -= newVal;

                    _counts[i] = newVal;
                    _availItems[i] = newAvail;
                }
                _counts[0] = _availItems[pos] - (pos * newVal);
                return _counts[1] <= _counts[0];
            }
        }

        #endregion // Inner Enumerator Class
    }
}
