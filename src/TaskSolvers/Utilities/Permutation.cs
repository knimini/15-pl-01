﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Diagnostics;

namespace TaskSolvers.Utilities
{
    public class Permutation
    {
        private int[] _lookupIndices;

        public Permutation(int[] indices)
        {
            if (indices == null) 
                throw new NullReferenceException("can't initialize permutation with null!");

            _lookupIndices = new int[indices.Length];
            indices.CopyTo(_lookupIndices, 0);
        }

        /// <summary>
        /// Constructs permutation from factoradixNumber in BigEndian form
        /// </summary>
        public Permutation(int[] factoradixNumber, int itemCount)
        {
            if (factoradixNumber == null)
                throw new NullReferenceException("can't initialize permutation with null!");
            
            List<int> indices = Enumerable.Range(0, factoradixNumber.Length).ToList();
            _lookupIndices = new int[itemCount];

            /* big endian form, so reverse iteration */
            for (int i = factoradixNumber.Length - 1; i >= 0; i--)
            {
                int factoradixDigit = factoradixNumber[i];
                _lookupIndices[i] = indices[factoradixDigit];
                indices.RemoveAt(factoradixDigit);
            }
        }

        public T[] PermuteArray<T>(T[] array)
        {
            T[] result = new T[array.Length];

            for (int i = 0; i < _lookupIndices.Length; i++)
                result[i] = array[_lookupIndices[i]];

            return result;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Permutation;

            if (other == null)
                return false;
            else
            {
                bool result = MoreLinq.ArrayEquals(_lookupIndices, other._lookupIndices);
                if (result)
                    return true;
                return false;
            }
        }

        public override int GetHashCode()
        {
            return _lookupIndices.Length ^ (_lookupIndices.Length >= 1 ? _lookupIndices[0] : Int32.MaxValue);
        }
    }
}
