﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities
{
    public static class TolerantComparer
    {
        public const double DEFAULT_TOLERANCE = 0.1;

        public static bool AreEqual(double val1, double val2)
        {
            return Math.Abs(val1 - val2) < DEFAULT_TOLERANCE;
        }

        public static bool Less(double lhv, double rhv, double tolerance = DEFAULT_TOLERANCE)
        {
            double diff = lhv - rhv;
            if (Math.Abs(diff) < tolerance)
                return false;
            else
                return diff < 0;
        }

        public static bool LessOrEqual(double lhv, double rhv, double tolerance = DEFAULT_TOLERANCE)
        {
            double diff = lhv - rhv;
            if (Math.Abs(diff) < tolerance)
                return true;
            else
                return diff < 0;
        }
    }
}
