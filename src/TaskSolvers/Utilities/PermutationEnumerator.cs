﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities
{
    /// <summary>
    /// this has a huge flow of being limited by Int64.MaxValue
    /// </summary>
    public class PermutationEnumerator : IEnumerator<Permutation>
    {
        private const int NOT_CALCULATED = -1;

        private int[] _factoNumber; /* factorradix - big endian form */
        private BigInteger _count;

        public BigInteger Count 
        { 
            get 
            {
                if (_count == NOT_CALCULATED)
                    _count = CalculateCount();
                return _count; 
            } 
        }

        public PermutationEnumerator(int itemCount)
        {
            _factoNumber = new int[itemCount];
            _count = NOT_CALCULATED;
            Reset();
        }

        object System.Collections.IEnumerator.Current { get { return Current; } }
        public Permutation Current { get { return new Permutation(_factoNumber, _factoNumber.Length); } }
        public void Dispose() { }

        public bool MoveNext()
        {
            for (int digit = 1 /* first is always 0 */; digit < _factoNumber.Length; digit++)
            {
                if (_factoNumber[digit] < digit)
                {
                    _factoNumber[digit] += 1;
                    return true;
                }
                _factoNumber[digit] = 0;
            }
            return false;
        }

        public void Reset()
        {
            Array.Clear(_factoNumber, 0, _factoNumber.Length);

            if (_factoNumber.Length > 1)
                _factoNumber[1] = -1;
        }

        private BigInteger CalculateCount()
        {
            BigInteger result = 1;
            for (int n = 2; n <= _factoNumber.Length; n++)
            {
                result = result * n;
            }
            return result;
        }
    }
}
