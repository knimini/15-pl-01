﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities
{
    /// <summary>
    /// Workaround class to have a reference to a value
    /// </summary>
    /// <remarks>
    /// In C++ a pointer would solve this problem, we could also make code unsafe
    /// but I'll just stick to this workaround.
    /// </remarks>
    [DebuggerDisplay("ref: {Value}")]
    sealed class Ref<T>
    {
        public T Value { get; set; }

        public Ref()
        {
            Value = default(T);
        }
        public Ref(T val)
        {
            Value = val;
        }
        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
