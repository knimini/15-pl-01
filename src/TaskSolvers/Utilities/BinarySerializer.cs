﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.Utilities
{
    public static class BinarySerializer
    {
        public static IFormatter binFormatter = new BinaryFormatter();

        public static byte[] Serialize(object obj)
        {
            byte[] bytes;
            using (MemoryStream stream = new MemoryStream())
            {
                binFormatter.Serialize(stream, obj);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public static void Serialize(object obj, Stream stream)
        {
            binFormatter.Serialize(stream, obj);
        }

        public static T Deserialize<T>(byte[] bytes)
        {
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                return (T)binFormatter.Deserialize(stream);
            }
        }

        public static T Deserialize<T>(Stream stream)
        {
            return (T)binFormatter.Deserialize(stream);
        }
    }
}
