﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;
using TaskSolvers.Utilities.Combinatorics;
using TaskSolvers.Utilities.Combinatorics.Distributions;
using UCCTaskSolver;

namespace TaskSolvers.DVRP
{
    public class DvrpBruteSolver : TaskSolver, IDvrpSolver
    {
        public static string ProblemName { get { return "DynamicVehicleRouting"; } }
        
        private DvrpProblem _problem;
        private byte[] _rawProblem;
        
        #region IDvrpSolver

        public override string Name { get { return ProblemName; } }

        /// <summary>
        /// Creates a DvrpSolver for Dividing or Solving
        /// </summary>
        /// <param name="serializedDvrpProblem">SerializedProblem if this solver will be used to divide problem, null if it's just for solving</param>
        public DvrpBruteSolver(byte[] serializedDvrpProblem)
            : base(serializedDvrpProblem)
        {
            if (serializedDvrpProblem != null)
            {
                _rawProblem = serializedDvrpProblem;
                _problem = BinarySerializer.Deserialize<DvrpProblem>(serializedDvrpProblem);
            }
        }

        public override byte[][] DivideProblem(int threadCount)
        {
            /* get all possible route-lengths-setups for solutions [length values exclude start-end depot] */
            List<LengthSetup> lengthSetups = GenerateSetups();

            /* split lengthSetups to different threads */
            LengthSetup[][] setupsPerThread = DivideSetups(lengthSetups, threadCount);

            /* serialize necessary data for each node */
            byte[][] result = new byte[threadCount][];
            using (MemoryStream ms = new MemoryStream())
            {
                byte[] rawProblem = BinarySerializer.Serialize(_problem);
                byte[] problemSize = BitConverter.GetBytes(rawProblem.Length);
                
                ms.Write(problemSize, 0, problemSize.Length);
                ms.Write(rawProblem, 0, rawProblem.Length);
                long setupsOffset = ms.Position;

                for (int tid = 0; tid < threadCount; tid++)
                {
                    WriteSetupsTo(ms, setupsPerThread[tid]);
                    result[tid] = ms.ToArray();
                    ms.Position = setupsOffset;
                }
            }
            
            return result;
        }

        public override byte[] MergeSolution(byte[][] solutions)
        {
            var bestSolution = new DvrpSolutionUnit(_problem);

            for (int i = 0; i < solutions.Length; i++)
            {
                var candidateSolution = BinarySerializer.Deserialize<DvrpSolutionUnit>(solutions[i]);
                if (/* no best soln yet */ bestSolution.TotalLength == 0.0 ||
                    /* better one found */ candidateSolution.CompareTo(bestSolution) == 1)
                {
                    bestSolution = candidateSolution;
                }
            }
            return BinarySerializer.Serialize(bestSolution);
        }

        public override byte[] Solve(byte[] partialData, TimeSpan timeout)
        {
            List<LengthSetup> setups; 
            DvrpProblemHeuristics heuristics;

            using (MemoryStream ms = new MemoryStream(partialData))
            using (BinaryReader reader = new BinaryReader(ms))
            {
                /* get problem  & heuristics */
                int rawProblemLength = reader.ReadInt32();
                using (MemoryStream ms2 = new MemoryStream(partialData, sizeof(Int32), rawProblemLength))
                {
                    _problem = BinarySerializer.Deserialize<DvrpProblem>(ms2);
                }
                heuristics = new DvrpProblemHeuristics(_problem);
                
                /* get setups */
                ms.Position = rawProblemLength + sizeof(Int32);
                setups = ReadSetupsFrom(ms);
            }

            /* prepare multi-threaded setup-filling */
            object syncRoot = new object();
            Ref<DvrpSolutionUnit> bestSolution = new Ref<DvrpSolutionUnit>();
            Ref<double> bestSolutionLength = new Ref<double>(Double.PositiveInfinity);
            int threadCount = Environment.ProcessorCount > _problem.ClientCount ?
                _problem.ClientCount : Environment.ProcessorCount;

            Task[] setupFillingTasks = new Task[threadCount];
            SetupSearchBoundary[] setupSearchBoundaries = GetSetupSearchBoundaries(threadCount);
            BrutalRouteFiller[] routeFillers = new BrutalRouteFiller[threadCount];
            for (int i = 0; i < threadCount; i++)
                routeFillers[i] = new BrutalRouteFiller(_problem, heuristics, syncRoot, bestSolutionLength, bestSolution);

            /* fill setups (one by one) for solution using n-threads for each setup */
            for(int i = 0; i < setups.Count; i++)
            {
                LengthSetup setup = setups[i];
                /* check if setup is feasible to beat best result */
                if (heuristics.IsFeasibleSetup(setup, bestSolutionLength.Value) == false)
                    continue;
                
                /* create tasks */
                for (int tid = 0; tid < threadCount; tid++)
                {
                    int index = tid;
                    setupFillingTasks[tid] = new Task(() =>
                        {
                            routeFillers[index].FillForLengthSetup(
                                setup,
                                setupSearchBoundaries[index].minFirstNode,
                                setupSearchBoundaries[index].maxFirstNode);
                        });
                }


                /* run and wait for all */
                for (int tid = 0; tid < threadCount; tid++)
                    setupFillingTasks[tid].Start();
                Task.WaitAll(setupFillingTasks);

                Debug.WriteLine("Setup #" + i.ToString() + " analized. Best Len: " + bestSolutionLength.ToString());
            }

            /* no feasible solution exists => make fake one with no routes */
            if (bestSolution.Value == null)
                bestSolution.Value = new DvrpSolutionUnit(_problem);

#if DEBUG
            Debug.WriteLine(String.Format("Skipped {0}/{1} [{2}%] setups!", 
                heuristics._skippedSetups, 
                setups.Count,
                100.0*(heuristics._skippedSetups / (double) setups.Count)));
#endif
            return BinarySerializer.Serialize(bestSolution.Value);
        }

        public DvrpSolutionUnit DeserializeSolution(byte[] mergedSolution)
        {
            return BinarySerializer.Deserialize<DvrpSolutionUnit>(mergedSolution);
        }

        #endregion // IDvrpSolver

        private List<LengthSetup> GenerateSetups()
        {
            var result = new List<LengthSetup>();
            for (int extraDepotVisits = 0; extraDepotVisits <= _problem.ClientCount - 1; extraDepotVisits++)
            {
                var distributor = ItemDistributorFactory.CreateDistributor(_problem.ClientCount + extraDepotVisits, _problem.VehicleCount, false, false);
                foreach (var dist in distributor)
                {
                    /* important note:
                     * due to implementation of distributor lengthSetups  'result' will end up sorted by
                     * extraVisits and then usedVehicles which lines up with amount of work needed
                     * to fill setup (last in the list = the most work)
                     */
                    result.Add(new LengthSetup(dist, extraDepotVisits));
                }
            }
            return result;
        }

        private LengthSetup[][] DivideSetups(IList<LengthSetup> setups, int threadCount)
        {
            int setupsPerThread = setups.Count / threadCount;
            int remainderSetups = setups.Count % threadCount;

            /* thread - setups */
            LengthSetup[][] result = new LengthSetup[threadCount][];
            for (int tid = 0; tid < threadCount; tid++)
            {
                if (tid < remainderSetups)
                    result[tid] = new LengthSetup[setupsPerThread + 1];
                else
                    result[tid] = new LengthSetup[setupsPerThread];

                int setupIndex = tid;
                for (int i = 0; i < result[tid].Length; i++)
                {
                    result[tid][i] = setups[setupIndex];
                    setupIndex += threadCount;
                }
            }
            return result;
        }

        private void WriteSetupsTo(MemoryStream ms, LengthSetup[] setups)
        {
            for (int i = 0; i < setups.Length; i++)
            {
                setups[i].PackToStream(ms);
            }
        }

        private List<LengthSetup> ReadSetupsFrom(MemoryStream ms)
        {
            List<LengthSetup> result = new List<LengthSetup>();

            while (ms.Position < ms.Length)
            {
                result.Add(new LengthSetup(ms));
            }
            return result;
        }
        
        #region Setup Search Boundaries (for concurrent threads)

        struct SetupSearchBoundary
        {
            public int minFirstNode;
            public int maxFirstNode;

            public SetupSearchBoundary(int minFirstNode, int maxFirstNode)
            {
                this.minFirstNode = minFirstNode;
                this.maxFirstNode = maxFirstNode;
            }
        }

        private SetupSearchBoundary[] GetSetupSearchBoundaries(int threadCount)
        {
            Debug.Assert(_problem != null);
            SetupSearchBoundary[] result = new SetupSearchBoundary[threadCount];

            /* get client node list (exclude depots) */
            List<int> clientNodes = new List<int>(_problem.ClientCount);
            for (int i = 0; i < _problem.StopPoints.Length; i++)
            {
                if (_problem.StopPoints[i].isDepot == false)
                    clientNodes.Add(i);
            }

            /* fill boudnaries */
            int nodesPerThread = clientNodes.Count / threadCount;
            int remainingNodes = clientNodes.Count % threadCount;
            int minFirstNodeIndex = 0, maxFirstNodeIndex;
            for (int tid = 0; tid < threadCount; tid++)
            {
                if (tid < remainingNodes)
                    maxFirstNodeIndex = minFirstNodeIndex + nodesPerThread;
                else
                    maxFirstNodeIndex = minFirstNodeIndex + nodesPerThread - 1;
                
                result[tid] = new SetupSearchBoundary(
                    clientNodes[minFirstNodeIndex], 
                    clientNodes[maxFirstNodeIndex]);
                minFirstNodeIndex = maxFirstNodeIndex + 1;
            }

            return result;
        }

        #endregion // Split Search Boundaries
    }
}
