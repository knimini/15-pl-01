﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.DVRP
{
    /// <summary>
    /// Represents a distributon of lengths in a Dvrp solution
    /// </summary>
    public struct LengthSetup
    {
        public int[] lengths;     // route lengths (without start-end depot)
        public int extraDepotVisits;   // extra depot visits

        public LengthSetup(int[] routeLengths, int extraDepotVisits)
        {
            this.lengths = routeLengths;
            this.extraDepotVisits = extraDepotVisits;
        }
        
        #region Packing

        public void PackToStream(Stream stream)
        {
            using (var writer = new BinaryWriter(stream, Encoding.ASCII, true))
            {
                writer.Write(extraDepotVisits);
                writer.Write(lengths.Length);
                for (int i= 0; i < lengths.Length; i ++)
                    writer.Write(lengths[i]);
            }
        }

        public LengthSetup(Stream stream)
        {
            using (BinaryReader reader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                this.extraDepotVisits = reader.ReadInt32();
                this.lengths = new int[reader.ReadInt32()];
                for (int i = 0; i < lengths.Length; i++)
                    lengths[i] = reader.ReadInt32();
            }
        }

        #endregion // ISerializable
    }
}
