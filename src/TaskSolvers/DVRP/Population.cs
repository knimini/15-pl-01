﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.DVRP
{
    /// <summary>
    /// Represents a collection of solution units. 
    /// This class always preserves units sorted by their fitness and exposes some handy properties.
    /// </summary>
    [Serializable]
    public class Population : IEnumerable<DvrpSolutionUnit>
    {
        private static Random rand = new Random();

        private LinkedList<DvrpSolutionUnit> _units;

        public int TotalSize { get { return _units.Count; } }
        public int FeasibleSize { get { return _units.Count(sUnit => sUnit.UnvisitedCount == 0); } }
        public DvrpSolutionUnit BestUnit { get { return _units.First.Value; } }
        public double AvarageLength
        {
            get
            {
                double lengthSum = 0;
                int i = 0;

                var enumerator = _units.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.UnvisitedCount > 0)
                        break;

                    lengthSum += enumerator.Current.TotalLength;
                    i += 1;
                }
                return lengthSum / (double)i;
            }
        }

        public Population(int size, DvrpProblem problem)
        {
            _units = new LinkedList<DvrpSolutionUnit>();
            for (int i = 0; i < size; i++)
                Add(GeneticOperations.SpawnSolution(problem));
        }

        public Population(IEnumerable<DvrpSolutionUnit> units)
        {
            _units = new LinkedList<DvrpSolutionUnit>(units);
        }

        #region Public Interface

        public void Add(DvrpSolutionUnit item)
        {
            if (_units.Count == 0)
            {
                _units.AddFirst(item);
                return;
            }

            var newNode = new LinkedListNode<DvrpSolutionUnit>(item);
            var curNode = _units.First;
            while (curNode != null)
            {
                // newItem fitness >= curItem fitness
                if (item.CompareTo(curNode.Value) >= 0)
                {
                    _units.AddBefore(curNode, newNode);
                    return;
                }
                curNode = curNode.Next;
            }

            _units.AddLast(newNode);
        }
        public void Clear() { _units.Clear(); }
        public void RemoveN00bs(int n00bCount)
        {
            for (int i = 0; i < n00bCount; i++)
                _units.RemoveLast();
        }

        /// <summary>
        /// Randomly picks mom and dad out of units collection.
        /// </summary>
        /// <param name="sortedUnits">Units sorted by their 'fitness' value. (last has highest fitness)</param>
        /// <remarks>
        /// Probability for the i-rank unit to be selected (i-th in the sorted collection)
        /// is i / (N+1)*N/2. In other words, they have same denominator and nominator increases together
        /// with rank.
        /// </remarks>
        public void RankSelection(out DvrpSolutionUnit mom, out DvrpSolutionUnit dad)
        {
            // get random values
            int denominator = (_units.Count + 1) * (_units.Count) / 2;
            int momRandValue = rand.Next(denominator) + 1;
            int dadRandValue = rand.Next(denominator) + 1;
            LinkedListNode<DvrpSolutionUnit> momNode = null, dadNode = null;

            // get ranks from random values
            int cumulativeSum = 0, curRank = 1, curProbability = _units.Count;
            var curNode = _units.First;
            // find mom's rank
            while (curNode != null)
            {
                cumulativeSum += curProbability;
                if (cumulativeSum >= momRandValue)
                {
                    momNode = curNode;
                    break;
                }
                else
                {
                    curRank += 1;
                    curProbability -= 1;
                }
            }
            // find dad's rank
            cumulativeSum = 0; curRank = 1; curProbability = _units.Count;
            curNode = _units.First;
            while (curNode != null)
            {
                cumulativeSum += curProbability;
                if (cumulativeSum >= dadRandValue)
                {
                    dadNode = curNode;
                    break;
                }
                else
                {
                    curRank += 1;
                    curProbability -= 1;
                }
            }

            if (momNode == dadNode)
                dadNode = momNode.Next != null ? momNode.Next : _units.First;

            mom = momNode.Value;
            dad = dadNode.Value;
        }

        #endregion // Public Interface

        #region IEnumerable

        public IEnumerator<DvrpSolutionUnit> GetEnumerator() { return _units.GetEnumerator(); }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return (_units as System.Collections.IEnumerable).GetEnumerator(); }

        #endregion // IEnumerable
    }
}
