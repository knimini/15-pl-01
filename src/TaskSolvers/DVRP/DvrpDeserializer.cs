﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TaskSolvers.Utilities;

namespace TaskSolvers.DVRP
{
    /// <summary>
    /// Class that allows deserialization of .vrp files into DvrpProblem objects.
    /// </summary>
    public class DvrpDeserializer
    {
        private const double DEFAULT_SPEED = 1.0;
        private const double CUT_OFF_PARAMETER = 0.5;

        private static readonly string CoordCaptureGroupName = "coords";
        private static readonly string DemandCaptureGroupName = "demands";
        private static readonly string NumDepotsCaptureGroupName = "numDepots";
        private static readonly string NumVisitsCaptureGroupName = "numVisits";
        private static readonly string NumVehiclesCaptureGroupName = "numVehicles";
        private static readonly string CapacityCaptureGroupName = "capacity";
        private static readonly string DepotsCaptureGroupName = "depots";
        private static readonly string TimeAvailCaptureGroupName = "timeAvail";
        private static readonly string DurationCaptureGroupName = "durations";
        private static readonly string DepotTimeWindowCaptureGroupName = "depotTimeWnd";

        private static List<Tuple<Token, int>> tokenAndGroupIdList;

        private static readonly string pattern;
        private static readonly Regex vrpRegex;
        private static Regex integerRegex;

        private int clientCount, depotCount, vehicleCount, vehicleCapacity;
        private Position[] positions;
        private int[] depotIds;
        private int[] demands;
        private int[] visitDurations;
        private int[] visitsAvailSince;
        private int[] visitsAvailUntil;
        private double vehicleSpeed;

        // ========= CONSTRUCTORS ==========
        static DvrpDeserializer() 
        {
            pattern =
                /* demand     token */ @"(?<" + DemandCaptureGroupName + @">DEMAND_SECTION(\s*[0-9]+ [-0-9]+)+)|" +
                /* loc coords token */ @"(?<" + CoordCaptureGroupName + @">LOCATION_COORD_SECTION(\s*-*\d+ -*\d+ -*\d+)+)|" +
                /* durations  token */ @"(?<" + DurationCaptureGroupName + @">DURATION_SECTION(\s*\d+ \d+)+)|" +
                /* num depots token */ @"(?<" + NumDepotsCaptureGroupName + @">NUM_DEPOTS:\s+\d+)|" +
                /* num visits token */ @"(?<" + NumVisitsCaptureGroupName + @">NUM_VISITS:\s+\d+)|" +
                /* num vehicl token */ @"(?<" + NumVehiclesCaptureGroupName + @">NUM_VEHICLES:\s+\d+)|" +
                /* capacity   token */ @"(?<" + CapacityCaptureGroupName + @">\sCAPACITIES:\s+\d+)|" +
                /* depots     token */ @"(?<" + DepotsCaptureGroupName + @">\sDEPOTS(\s+\d+)+)|"+
                /* depot TWnd token */ @"(?<" + DepotTimeWindowCaptureGroupName + @">\sDEPOT_TIME_WINDOW_SECTION(\s+\d+ \d+ \d+)+)|" +
                /* time avail token */ @"(?<" + TimeAvailCaptureGroupName + @">TIME_AVAIL_SECTION(\s*\d+ \d+)+)";

            vrpRegex = new Regex(pattern);
            integerRegex = new Regex(@"-*\d+");

            tokenAndGroupIdList = new List<Tuple<Token, int>>()
            {
                Tuple.Create<Token, int>(Token.Coords,  vrpRegex.GroupNumberFromName(CoordCaptureGroupName)),
                Tuple.Create<Token, int>(Token.Demands, vrpRegex.GroupNumberFromName(DemandCaptureGroupName)),
                Tuple.Create<Token, int>(Token.DepotCount, vrpRegex.GroupNumberFromName(NumDepotsCaptureGroupName)),
                Tuple.Create<Token, int>(Token.Capacity, vrpRegex.GroupNumberFromName(CapacityCaptureGroupName)),
                Tuple.Create<Token, int>(Token.VehicleCount, vrpRegex.GroupNumberFromName(NumVehiclesCaptureGroupName)),
                Tuple.Create<Token, int>(Token.VisitsCount, vrpRegex.GroupNumberFromName(NumVisitsCaptureGroupName)),
                Tuple.Create<Token, int>(Token.Depots, vrpRegex.GroupNumberFromName(DepotsCaptureGroupName)),
                Tuple.Create<Token, int>(Token.TimeAvail, vrpRegex.GroupNumberFromName(TimeAvailCaptureGroupName)),
                Tuple.Create<Token, int>(Token.DepotTimeWindows, vrpRegex.GroupNumberFromName(DepotTimeWindowCaptureGroupName)),
                Tuple.Create<Token, int>(Token.Duration, vrpRegex.GroupNumberFromName(DurationCaptureGroupName))
            };
        }

        public DvrpDeserializer()
        {
            ResetFieldValues();
        }

        // =============== DESERIALIZE MAIN METHOD =================
        public DvrpProblem DeserializeProblem(string input)
        {
            // reset all variables
            ResetFieldValues();
            
            var matches = vrpRegex.Matches(input);

            // process entire string - token by token
            foreach (Match match in matches)
	        {
                // find which token was captured
                foreach (Tuple<Token, int> tuple in tokenAndGroupIdList)
                {
                    int tokenGroupId = tuple.Item2;

                    // captured that token - process and find next
                    if (match.Groups[tokenGroupId].Success == true)
                    {
                        string tokenValue = match.Value;
                        switch (tuple.Item1)
                        {
                            case Token.Depots:
                                ProcessDepotsToken(tokenValue);
                                break;
                            case Token.Coords:
                                ProcessCoordsToken(tokenValue);
                                break;
                            case Token.Demands:
                                ProcessDemandToken(tokenValue);
                                break;
                            case Token.DepotCount:
                                ProcessDepotCountToken(tokenValue);
                                break;
                            case Token.DepotTimeWindows:
                                ProcessDepotTimeWindowsToken(tokenValue);
                                break;
                            case Token.Duration:
                                ProcessDurationToken(tokenValue);
                                break;
                            case Token.Capacity:
                                ProcessCapacityToken(tokenValue);
                                break;
                            case Token.VisitsCount:
                                ProcessVisitsCountToken(tokenValue);
                                break;
                            case Token.VehicleCount:
                                ProcessVehicleCountToken(tokenValue);
                                break;
                            case Token.TimeAvail:
                                ProcessTimeAvailableToken(tokenValue);
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                }
	        }

            return CreateDvrpProblemFromFieldValues();
        }

        // ================ TOKEN PROCESSING ==================
        private void ProcessDemandToken(string tokenValue)
        {
            if (clientCount == -1 || depotCount  == -1)
                throw new InvalidOperationException(
                    "Cannot deserialize problem, because it has incorrect format." + 
                    "visit count and depot count are expected to appear before demand_section");

            MatchCollection matches = integerRegex.Matches(tokenValue);
            demands = new int[clientCount + depotCount];
            for (int i = 0; i < matches.Count; i+=2)
            {
                int id = Int32.Parse(matches[i].Value);
                int demand = Int32.Parse(matches[i+1].Value);
                demand = Math.Abs(demand);

                demands[id] = demand;
            }
        }

        private void ProcessCoordsToken(string tokenValue)
        {
            if (clientCount == -1 || depotCount == -1)
                throw new InvalidOperationException(
                    "Cannot deserialize problem, because it has incorrect format." +
                    "visit count and depot count are expected to appear before demand_section");

            // store position array
            positions = new Position[clientCount + depotCount];
            MatchCollection matches = integerRegex.Matches(tokenValue);
            for (int i = 0; i < matches.Count; i += 3)
            {
                int id = Int32.Parse(matches[i].Value);
                int x = Int32.Parse(matches[i + 1].Value);
                int y = Int32.Parse(matches[i + 2].Value);

                positions[id] = new Position(x, y);
            }
        }

        private void ProcessDepotsToken(string tokenValue)
        {
            MatchCollection matches = integerRegex.Matches(tokenValue);

            depotIds = new int[matches.Count];
            for (int i = 0; i < matches.Count; i++)
            {
                depotIds[i] = Int32.Parse(matches[i].Value);
            }
        }

        private void ProcessDepotCountToken(string tokenValue)
        {
            // store depot count
            depotCount = Int32.Parse(integerRegex.Match(tokenValue).Value);
        }

        private void ProcessDurationToken(string tokenValue)
        {
            if (clientCount == -1 || depotCount == -1)
                throw new InvalidOperationException(
                    "Cannot deserialize problem, because it has incorrect format." +
                    "visit count and depot count are expected to appear before duration_section");

            MatchCollection matches = integerRegex.Matches(tokenValue);
            visitDurations = new int[clientCount + depotCount];
            for (int i = 0; i < matches.Count; i += 2)
            {
                int id = Int32.Parse(matches[i].Value);
                int duration = Int32.Parse(matches[i+1].Value);

                visitDurations[id] = duration;
            }
        }

        private void ProcessDepotTimeWindowsToken(string tokenValue)
        {
            if (clientCount == -1 || depotCount == -1)
                throw new InvalidOperationException(
                    "Cannot deserialize problem, because it has incorrect format." +
                    "visit count and depot count are expected to appear before depot_time_window_section");

            if (visitsAvailSince == null)
                visitsAvailSince = new int[clientCount + depotCount];
            if (visitsAvailUntil == null)
                visitsAvailUntil = new int[clientCount + depotCount];

            MatchCollection matches = integerRegex.Matches(tokenValue);
            for (int i = 0; i < matches.Count; i += 3)
            {
                int id = Int32.Parse(matches[i].Value);
                int since = Int32.Parse(matches[i + 1].Value);
                int until = Int32.Parse(matches[i + 2].Value);

                visitsAvailSince[id] = since;
                visitsAvailUntil[id] = until;
            } 
        }

        private void ProcessVisitsCountToken(string tokenValue)
        {
            // store client coutnt
            clientCount = Int32.Parse(integerRegex.Match(tokenValue).Value);
        }

        private void ProcessVehicleCountToken(string tokenValue)
        {
            // store vehicle count
            vehicleCount = Int32.Parse(integerRegex.Match(tokenValue).Value);
        }

        private void ProcessCapacityToken(string tokenValue)
        {
            // store vehicle capacity
            vehicleCapacity = Int32.Parse(integerRegex.Match(tokenValue).Value);
        }

        private void ProcessTimeAvailableToken(string tokenValue)
        {
            if (clientCount == -1 || depotCount == -1)
                throw new InvalidOperationException(
                    "Cannot deserialize problem, because it has incorrect format." +
                    "visit count and depot count are expected to appear before time_avail_section");

            MatchCollection matches = integerRegex.Matches(tokenValue);
            visitsAvailSince = new int[clientCount + depotCount];
            for (int i = 0; i < matches.Count; i += 2)
            {
                int id = Int32.Parse(matches[i].Value);
                int availSince = Int32.Parse(matches[i + 1].Value);

                visitsAvailSince[id] = availSince;
            }
        }

        // ================ HELPER FUNCTIONS =================
        private static double EuclidianDistance(Position a, Position b)
        {
            return Math.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
        }

        private DvrpProblem CreateDvrpProblemFromFieldValues()
        {
            DvrpProblem result = new DvrpProblem();

            result.ClientCount = clientCount;
            result.DepotIds = depotIds;
            result.DistanceMatrix = CalculateDistanceMatrix();
            result.StopPoints = CreateStopPoints();
            result.VehicleCount = vehicleCount;
            result.VehicleCapacity = vehicleCapacity;
            result.VehicleSpeed = vehicleSpeed;

            // enable constraints 
            result.CapacityConstraint = vehicleCapacity != -1;
            result.TimeConstraints = (visitsAvailSince != null);

            if (ValidateDvrpProblem(result) == false)
                throw new InvalidOperationException("The problem ill-defined.");

            return result;
        }

        private double[,] CalculateDistanceMatrix()
        {
            double[,] distanceMx = new double[positions.Length, positions.Length];

            for (int row = 0; row < positions.Length; row++)
            {
                for (int col = 0; col < positions.Length; col++)
                {
                    distanceMx[row, col] = EuclidianDistance(positions[row], positions[col]);
                }
            }

            return distanceMx;
        }

        private StopPoint[] CreateStopPoints()
        {
            var result = new StopPoint[depotCount + clientCount];

            // time constraints included in problem
            if (visitsAvailSince != null && visitsAvailUntil != null && visitDurations != null)
            {
                int lastDepotClose = depotIds.Max((dId) => visitsAvailUntil[dId]);
                int cutoffTime = (int) Math.Ceiling(CUT_OFF_PARAMETER * lastDepotClose); /* inclusive cutoff time */

                for (int i = 0; i < result.Length; i++)
                {
                    if (depotIds.Contains(i) == false)
                    {
                        int clientAvailSince = visitsAvailSince[i] >= cutoffTime ? 0 : visitsAvailSince[i];
                        result[i] = new StopPoint(demands[i], visitDurations[i], clientAvailSince, visitsAvailUntil[i], false /* not depot */);
                    }
                    else
                    {
                        result[i] = new StopPoint(demands[i], visitDurations[i], visitsAvailSince[i], visitsAvailUntil[i], true /* depot */);
                    }
                }
            }
            // no time constraints
            else
            {
                for (int i = 0; i < result.Length; i++)
                    result[i] = new StopPoint(demands[i], depotIds.Contains(i));
            }

            return result;
        }

        private void ResetFieldValues()
        {
            this.clientCount = -1;
            this.depotCount = -1;
            this.depotCount = -1;
            this.vehicleCapacity = -1;
            this.vehicleCount = -1;
            this.vehicleSpeed = DEFAULT_SPEED;

            this.depotIds = null;
            this.positions = null;
            this.visitsAvailSince = null;
            this.visitsAvailUntil = null;
            this.demands = null;
        }

        private bool ValidateDvrpProblem(DvrpProblem problem)
        {
            // depot's demand must be zero
            return problem.DepotIds.All((sId) => problem.StopPoints[sId].demand == 0 && problem.StopPoints[sId].isDepot == true);
        }

        // ============ INNER CLASSES (position, token) ==========
        [DebuggerDisplay("pos: ({x}, {y})")]
        private struct Position
        {
            public int x, y;
            public Position(int x, int y) { this.x = x; this.y = y; }
        }

        private enum Token
        {
            Coords,
            Demands,
            DepotCount,
            Depots,
            DepotTimeWindows,
            Duration,
            VisitsCount,
            VehicleCount,
            Capacity,
            TimeAvail
        };
    }
}
