﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.DVRP
{
    /// <summary>
    /// Class representing a solution to DVRP problem. 
    /// </summary>
    /// <remarks>
    /// Class implements IComparable, since solutions can be compared using their fitness value.
    /// 
    /// Solution may be 'incomplete', meaning that not all clients are 
    /// visited by this solution. 
    /// </remarks>
    [DebuggerDisplay("Length: {TotalLength}    Routes: {Routes.Count}")]
    [Serializable]
    public class DvrpSolutionUnit : IComparable<DvrpSolutionUnit>
    {
        private DvrpProblem _problem;
        private List<Route> _routes;
        private HashSet<int> _unvisitedNodes;

        #region Properties

        public DvrpProblem Problem { get { return _problem; } set { _problem = value; } }                        // Problem which this unit solves
        public HashSet<int> UnvisitedNodes { get { return _unvisitedNodes; } set { _unvisitedNodes = value; } }  // Client nodes which are (currently) not visited
        public int UnvisitedCount { get { return UnvisitedNodes.Count; } }                                       // Number of unvisited clients
        public IReadOnlyList<Route> Routes { get { return _routes.AsReadOnly(); } }                                // Routes defining this solution
        public double TotalLength { get { return _routes.Sum(route => route.Length); } }                          // Length of this solution

        #endregion // Properties

        #region Constructor

        public DvrpSolutionUnit(DvrpProblem problem)
        {
            _problem = problem;
            // routes = empty list
            _routes = new List<Route>();
            // unvisited nodes = empty set [because this solution is yet to be initialized with genetic algorythm]
            _unvisitedNodes = new HashSet<int>();
        }

        public DvrpSolutionUnit(DvrpProblem problem, IList<Route> routes)
        {
            _problem = problem;
            _routes = new List<Route>(routes.Select(r => r.Clone()));

            _unvisitedNodes = new HashSet<int>(Enumerable.Range(0, problem.StopPoints.Length));
            foreach (var route in routes)
                foreach (var arc in route)
                    _unvisitedNodes.Remove(arc.From);
        }

        #endregion // Constructor

        #region Public Interface

        public List<RouteArc> GetAllRouteArcs()
        {
            List<RouteArc> result = new List<RouteArc>(_problem.StopPoints.Length);
            foreach (var route in Routes)
                result.AddRange(route);
            return result;
        }

        public Route CreateNewRoute()
        {
            var result = new Route(_problem);
            _routes.Add(result);
            return result;
        }

        public void AddNewRoute(Route newRoute)
        {
            _routes.Add(newRoute);
        }

        public bool RemoveNode(int node)
        {
            foreach (var route in Routes)
            {
                if (route.RemoveNode(node) == true)
                {
                    UnvisitedNodes.Add(node);
                    return true;
                }
            }
            return false;
        }

        public DvrpSolutionUnit Clone()
        {
            DvrpSolutionUnit clone = new DvrpSolutionUnit(_problem);

            foreach (var route in Routes)
                clone._routes.Add(route.Clone());
            clone.UnvisitedNodes = new HashSet<int>(UnvisitedNodes);

            return clone;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(String.Format("=== soln length : {0} ======= unvisited nodes: {0} ===", TotalLength, UnvisitedCount));

            for (int i = 0; i < _routes.Count; i++)
                sb.AppendLine(String.Format("ROUTE {0} : {1}", i, _routes[i].ToString()));

            return sb.ToString();
        }

        #endregion // Public Interface

        #region IComparable

        public int CompareTo(DvrpSolutionUnit other)
        {
            // solution with more assigned nodes has higher 'fitness'
            if (this.UnvisitedCount != other.UnvisitedCount)
            {
                return (this.UnvisitedCount < other.UnvisitedCount) ?
                    /* assigned more => better */ 1 :
                    /* assigned less => worse */ -1;
            }

            // solution with shorter route has higher 'fitness'
            return -(TotalLength.CompareTo(other.TotalLength));
        }

        #endregion // IComparable

        #region Serializable

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            foreach (var route in Routes)
                route.Problem = this._problem;
        }

        #endregion // Serializable
    }
}
