﻿using System;
using System.Diagnostics;

namespace TaskSolvers.DVRP
{
    [DebuggerDisplay("{From} => {To}   L:{Load}")]
    [Serializable]
    public class RouteArc
    {
        [NonSerialized]
        private Route _route;

        public Route Route { get { return _route; } set { _route = value; } }  // route to which this arc belongs

        public RouteArc Previous { get; set; }              // previours RouteArc in the route
        public RouteArc Next { get; set; }                  // next RouteArc in the route
        public int Load { get; set; }                       // amount of cargo being moved
        public int From { get; set; }                       // vertex from which this arc starts
        public int To { get; set; }                         // vertex to which this arc goes
        public double DelayTime { get; set; }               // traveling this arc takes more time than it has to
        public double ArrivalAtDestination { get; set; }    // time at which we arrive to destination

        public double ArrivalAtSource                       // time at which we arrive to source
        {
            get { return Previous == null ? 0 : Previous.ArrivalAtDestination; }
        }
        public double DepartureFromSource                   // time of departure from source
        {
            get
            {
                return Previous == null ?
                    0 :
                    Previous.ArrivalAtDestination + _route.Problem.StopPoints[From].unloadDuration;
            }
        }

        public RouteArc(int from, int to)
        {
            From = from;
            To = to;
        }

        public RouteArc(int from, int to, int load)
        {
            Load = load;
            To = to;
            From = from;
        }
    }
}
