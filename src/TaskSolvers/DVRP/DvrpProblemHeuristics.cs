﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskSolvers.DVRP
{
    class DvrpProblemHeuristics
    {
        // precalculated sum of length of n-shortest arcs
        // e.g. _summedArcs[2] is equal to length of 2 shortest arcs
        // e.g. _summedUnloadTime[2] is minUnloadTime for two client nodes
        double[] _summedArcs;
        // int[] _summedUnloadTime;

        int _minClientDemand;
        int _vehicleCapacity;
        double _vehicleSpeed;

#if DEBUG
        public int _skippedSetups;
#endif

        #region Construction

        public DvrpProblemHeuristics(DvrpProblem problem)
        {
            /* shortest arcs */
            InitializeSummedArcs(problem);

            /* minimum unload duration & demand */
            // InitializeUnloads(problem);
            _minClientDemand = problem.StopPoints.Min(sp => sp.isDepot ? Int32.MaxValue : sp.demand);

            /* vehicle capacity */
            _vehicleCapacity = problem.VehicleCapacity;
            _vehicleSpeed = problem.VehicleSpeed;
        }

        private void InitializeSummedArcs(DvrpProblem problem)
        {
            /* get shortest arcs from problem */
            List<double> shortestArcs = new List<double>(problem.ClientCount - 1);
            for (int clientId = 0; clientId < problem.StopPoints.Length; clientId++)
            {
                if (problem.IsDepot(clientId) == true)
                    continue;

                double shortest = Double.PositiveInfinity;
                double shortest2 = Double.PositiveInfinity;
                for (int to = 0; to < problem.ClientCount; to++)
                {
                    if (to == clientId)
                        continue;
                    
                    double dist = problem.DistanceMatrix[clientId, to];
                    if (dist < shortest2)
                    {
                        if (dist < shortest)
                        {
                            shortest2 = shortest;
                            shortest = dist;
                        }
                        else
                        {
                            shortest2 = dist;
                        }
                    }
                }
                shortestArcs.Add(shortest);
                shortestArcs.Add(shortest2);
            }
            shortestArcs.Sort();

            /* precalculate sums */
            _summedArcs = new double[shortestArcs.Count + 1];
            for (int i = 1; i < _summedArcs.Length; i++)
                _summedArcs[i] += _summedArcs[i - 1] + shortestArcs[i - 1];
        }

        //private void InitializeUnloads(DvrpProblem problem)
        //{
        //    List<int> unloadTimes = new List<int>(problem.ClientCount);
        //    foreach (var sp in problem.StopPoints)
        //    {
        //        if (sp.isDepot == true)
        //            continue;

        //        unloadTimes.Add(sp.unloadDuration);
        //    }

        //    unloadTimes.Sort();
        //    _summedUnloadTime = new int[problem.ClientCount + 1];
        //    for (int i = 1; i < _summedUnloadTime.Length; i++)
        //        _summedUnloadTime[i] += _summedUnloadTime[i - 1] + unloadTimes[i - 1];
        //}

        #endregion // Construction

        #region Heuristic functions

        private int ExtraVisitSpots(LengthSetup setup)
        {
            int result = 0;
            foreach (var rlen in setup.lengths)
            {
                result += (rlen - 1) / 2;
            }
            return result;
        }

        private double MinimumSetupLength(LengthSetup lengthSetup)
        {
            int minDistinctArcs = 0;
            int maxDoubledArcs = 0;

            foreach (var routeLen in lengthSetup.lengths)
            {
                /* empty routes left */
                if (routeLen == 0)
                    break;
                /* first and last arc */
                else if (routeLen == 1)
                {
                    minDistinctArcs += 1;
                    maxDoubledArcs += 1;
                }
                else
                    minDistinctArcs += 2;
                
                /* middle arcs (corrected later) */
                minDistinctArcs += routeLen - 1;
            }

            /* correction for extra visits */
            // extra visit may look like: ...d->c->d...
            minDistinctArcs -= lengthSetup.extraDepotVisits;
            maxDoubledArcs += lengthSetup.extraDepotVisits;

            /* return minimum bound for this setup */
            return _summedArcs[minDistinctArcs] +
                _summedArcs[maxDoubledArcs];
        }

        private int MinimumCapacityRequirement(LengthSetup lengthSetup)
        {
            int longestRouteLen = lengthSetup.lengths.Max(l => l);
            int clientsInRoute = longestRouteLen - lengthSetup.extraDepotVisits;
            int minClientsBetweenDepots = (int) Math.Floor(clientsInRoute / (double)(lengthSetup.extraDepotVisits + 1));
            return minClientsBetweenDepots * _minClientDemand;
        }

        public bool IsFeasibleSetup(LengthSetup lengthSetup, double distanceToBeat)
        {
            /* feasible spot count >= target extra visits */
            if (ExtraVisitSpots(lengthSetup) < lengthSetup.extraDepotVisits)
            {
                IncrementSkipped();
                return false;
            }
            /* minimum length < distanceToBeat */
            if (MinimumSetupLength(lengthSetup) > distanceToBeat)
            {
                IncrementSkipped();
                return false;
            }
            /* min capacity < vehicle capacity */
            if (MinimumCapacityRequirement(lengthSetup) > _vehicleCapacity)
            {
                IncrementSkipped();
                return false;
            }

            return true;
        }

        #endregion // Heuristic functions

        [Conditional("DEBUG")]
        private void IncrementSkipped()
        {
            #if DEBUG
            _skippedSetups += 1;
            #endif
        }
    }
}
