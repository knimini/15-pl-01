﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;
using UCCTaskSolver;

namespace TaskSolvers.DVRP
{
    public class DvrpGeneticSolver : TaskSolver, IDvrpSolver
    {
        #region Algorythm constants

        public static readonly TimeSpan MAIN_POPULATION_DEFAULT_TIMEOUT = TimeSpan.FromMinutes(5);

        public const int MAIN_POPULATION_SIZE = 100;
        public const int MAIN_POPULATION_MAX_PROGRSSLESS_ITERATIONS = 50;
        public const int MAIN_POPULATION_BREEDING_SESSIONS = 10;

        public const int SUB_POPULATION_SIZE = 20;
        public const int SUB_POPULATION_MAX_PROGRESSLESS_ITERATIONS = 5;
        public const int SUB_POPULATION_BREEDING_SESSIONS = 2;

        public const double MUTATION_PROBABILITY = 0.1;

        #endregion // Algorythm constants

        private static Random rand = new Random();

        private DvrpProblem _problem;
        private byte[] _rawProblem;

        public override string Name { get { return "DynamicVehicleRouting"; } }

        public DvrpGeneticSolver(byte[] data)
            : base(data)
        {
            _rawProblem = data;
            _problem = BinarySerializer.Deserialize<DvrpProblem>(data);
        }

        public override byte[][] DivideProblem(int threadCount)
        {
            // herpa derp - not in this algorythm dawg.
            // every thread will just runs the same shit here.

            byte[][] result = new byte[threadCount][];
            for (int i = 0; i < threadCount; i++)
            {
                result[i] = new byte[_rawProblem.Length];
                _rawProblem.CopyTo(result[i], 0);
            }

            return result;
        }

        public override byte[] MergeSolution(byte[][] solutions)
        {
            Population[] populations = new Population[solutions.Length];
            for (int i = 0; i < solutions.Length; i++)
                populations[i] = BinarySerializer.Deserialize<Population>(solutions[i]);

            DvrpSolutionUnit bestFromAllPops = populations.Select((sPop) => sPop.BestUnit).MaxBy((sUnit) => sUnit);
            return BinarySerializer.Serialize(bestFromAllPops);
        }

        // Algorythm based on Gintaras Vaira's genetic algorythm for DVRP
        public override byte[] Solve(byte[] partialData, TimeSpan userSpecifiedTimeout)
        {
            DvrpProblem problem = BinarySerializer.Deserialize<DvrpProblem>(partialData);
            Population pop = new Population(MAIN_POPULATION_SIZE, problem);
            Population mutantPop;

            TimeSpan timeout = MAIN_POPULATION_DEFAULT_TIMEOUT < userSpecifiedTimeout ?
                MAIN_POPULATION_DEFAULT_TIMEOUT : userSpecifiedTimeout;

            // do some breeding iterations
            DateTime startTime = DateTime.Now;
            int progresslessIterations = 0;
            double bestLengthSoFar = pop.BestUnit.TotalLength;
            while (progresslessIterations < MAIN_POPULATION_MAX_PROGRSSLESS_ITERATIONS &&
                   DateTime.Now - startTime < timeout)
            {
                // remove spare units (until pop has desired size)
                int unitsToKillCount = pop.TotalSize - MAIN_POPULATION_SIZE;
                pop.RemoveN00bs(unitsToKillCount);

                // do reproduction
                DvrpSolutionUnit mom, dad, son, daughter, bestMutant;
                for (int i = 0; i < MAIN_POPULATION_BREEDING_SESSIONS; i++)
                {
                    pop.RankSelection(out mom, out dad);
                    daughter = GeneticOperations.CommonNodesCrossover(mom, dad);
                    son = GeneticOperations.CommonNodesCrossover(dad, mom);
                    pop.Add(son);
                    pop.Add(daughter);

                    // from time to time also mutate
                    if (rand.NextDouble() < MUTATION_PROBABILITY)
                    {
                        mutantPop = new Population(GeneticOperations.SpawnMultipleMutants(son, SUB_POPULATION_SIZE));
                        bestMutant = ProcessMutantPopulation(mutantPop);
                        pop.Add(bestMutant);
                    }
                }

                // check if reproduction brought any results
                if (pop.BestUnit.TotalLength < bestLengthSoFar)
                {
                    progresslessIterations = 0;
                    bestLengthSoFar = pop.BestUnit.TotalLength;
                }
                else
                {
                    progresslessIterations += 1;
                }
            }

            // return best result
            return BinarySerializer.Serialize(pop);
        }

        public DvrpSolutionUnit ProcessMutantPopulation(Population mutantPop)
        {
            int progresslessIterations = 0;
            double bestLength = mutantPop.BestUnit.TotalLength;

            // do some series of breeding
            while (progresslessIterations < SUB_POPULATION_MAX_PROGRESSLESS_ITERATIONS)
            {
                int mutantsToKill = mutantPop.TotalSize - SUB_POPULATION_SIZE;
                mutantPop.RemoveN00bs(mutantsToKill);

                // do reproduction
                DvrpSolutionUnit mom, dad, son, daughter, doubleMutant;
                for (int i = 0; i < SUB_POPULATION_BREEDING_SESSIONS; i++)
                {
                    mutantPop.RankSelection(out mom, out dad);
                    daughter = GeneticOperations.CommonNodesCrossover(mom, dad);
                    son = GeneticOperations.CommonNodesCrossover(dad, mom);
                    mutantPop.Add(son);
                    mutantPop.Add(daughter);

                    // sometimes mutate the mutant
                    if (rand.NextDouble() < MUTATION_PROBABILITY)
                    {
                        doubleMutant = GeneticOperations.SubsetReinsertionMutation(mom);
                        mutantPop.Add(doubleMutant);
                    }
                }

                // see if reproducing brought any improvement
                if (mutantPop.BestUnit.TotalLength < bestLength)
                {
                    progresslessIterations = 0;
                    bestLength = mutantPop.BestUnit.TotalLength;
                }
                else
                {
                    progresslessIterations += 1;
                }
            }

            return mutantPop.BestUnit;
        }

        public DvrpSolutionUnit DeserializeSolution(byte[] mergedSolution)
        {
            return (BinarySerializer.Deserialize<DvrpSolutionUnit>(mergedSolution));
        }
    }
}
