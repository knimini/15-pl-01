﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCCTaskSolver;
using TaskSolvers.Utilities;

namespace TaskSolvers.DVRP
{
    public interface IDvrpSolver
    {
        DvrpSolutionUnit DeserializeSolution(byte[] mergedSolution);
    }
}
