﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;
using TaskSolvers.Utilities.Combinatorics;

namespace TaskSolvers.DVRP
{
    class BrutalRouteFiller
    {
        DvrpProblem _problem;
        DvrpProblemHeuristics _heuristics;
        int _nextToInsert;
        Route _curFilledRoute;
        int _curFilledRouteIndex;

        /* accessed from multiple threads */
        object _syncRoot;
        Ref<DvrpSolutionUnit> _bestSolution;
        Ref<double> _bestSolutionLength;

        double _totalLengthSoFar;
        bool[] _visitedPoints;
        Route[] _routes;
        int _targetExtraDepotVisits;
        int _extraVisitCounter;
#if DEBUG
        long _id;
#endif

        /// <summary>
        /// Creates a new route filler to run in async environment
        /// </summary>
        /// <param name="problem">Problem being filled</param>
        /// <param name="syncRoot">Sync root (".NET mutex") for bestSolution & bestSolnLen updates</param>
        /// <param name="bestSolutionLength">Reference to bestSolutionLength</param>
        /// <param name="bestSolution">Reference to best solution</param>
        public BrutalRouteFiller(DvrpProblem problem, DvrpProblemHeuristics heuristics,
            object syncRoot, Ref<double> bestSolutionLength, Ref<DvrpSolutionUnit> bestSolution)
        {
            _syncRoot = syncRoot;
            _bestSolutionLength = bestSolutionLength;
            _bestSolution = bestSolution;

            _problem = problem;
            _heuristics = heuristics;
            _visitedPoints = new bool[_problem.StopPoints.Length];

            _routes = new Route[_problem.VehicleCount];
            for (int i = 0; i < _routes.Length; i++)
                _routes[i] = new Route(_problem);
        }

        /// <summary>
        /// Fills routes to fit a specified setup. Result is stored in the _bestSolutionReference passed in constructor
        /// </summary>
        /// <param name="lengthSetup">Length setup to fill for</param>
        /// <param name="minFirstNode">Minimum value of first client-node (inclusive)</param>
        /// <param name="maxFirstNode">Maximum value of first client-node (inclusive)</param>
        public void FillForLengthSetup(LengthSetup lengthSetup, int minFirstNode, int maxFirstNode)
        {
            /* reset&clear variables */
            _targetExtraDepotVisits = lengthSetup.extraDepotVisits;
            int usedRoutes = lengthSetup.lengths.Count((rLen) => rLen > 0);
            var startCombinations = new Combinations<int>(_problem.DepotIds, usedRoutes, GenerateOption.WithRepetition);
            var endCombinations = new Combinations<int>(_problem.DepotIds, usedRoutes, GenerateOption.WithRepetition);
            
            /* foreach start & finish combination - fill routes */
            foreach (var startCombination in startCombinations)
            {
                Debug.WriteLine("NEW SETUP: " + startCombination.ToString());
                Debug.WriteLine("EXTRA VISITS: " + _targetExtraDepotVisits);
                foreach (var endCombination in endCombinations)
                {
                    /* ========= ROUTE-COMBINATION HEURISTICS =========== */
                    // if (IsFeasibleCombination(lengthSetup.lengths, usedRoutes, _targetExtraDepotVisits, startCombination, endCombination) == false)
                    //      goto NextCombination;

                    /* ========= FILLINIG ROUTE FOR THIS COMBINATION ========== */
                    Array.ForEach(_routes, (r) => r.ClearRoute());
                    Array.Clear(_visitedPoints, 0, _visitedPoints.Length);
                    _nextToInsert = NextPossibleValueHigherThan(minFirstNode - 1);
                    _curFilledRouteIndex = 0;
                    _curFilledRoute = _routes[_curFilledRouteIndex];
                    _extraVisitCounter = 0;
                    _totalLengthSoFar = 0.0;
                    
                    /* initialize routes start-ends */
                    for (int index = 0; index < usedRoutes; index ++)
                    {
                        _routes[index].InsertFirstLap(startCombination[index], endCombination[index]);
                    }

                    /* try to fill the rest of route spots (until desired length) */
                    while (true)
                    {
                    NewNextInsert: 
                        /* if I am past my check-range, go to next combination */
                        if (_routes[0].NodeCount > 2 &&
                            _routes[0].First.To > maxFirstNode)
                            goto NextCombination;

                        /* don't make routes with depot-depot arcs - they are clearly subobtimal */
                        while (_problem.IsDepot(_curFilledRoute.OneBeforeLastNode) &&
                               _problem.IsDepot(_nextToInsert))
                        {
                            _nextToInsert = NextPossibleValueHigherThan(_nextToInsert);
                            if (_nextToInsert == -1)
                            {
                                if (PopAndPrepareForNextInsert() == true)
                                    goto NewNextInsert;
                                else
                                    goto NextCombination;
                            }    
                        }

                        /* insertion-not feasible => don't continue this way */
                        if (_curFilledRoute.IsFeasibleInsertion(_curFilledRoute.Last, _nextToInsert) == false)
                        {
                            _nextToInsert = NextPossibleValueHigherThan(_nextToInsert);
                            if (_nextToInsert == -1)
                            {
                                if (PopAndPrepareForNextInsert() == true)
                                    goto NewNextInsert;
                                else
                                    goto NextCombination;
                            }
                            goto NewNextInsert;
                        }

                        /* make an insertion */
                        Debug.Assert(_nextToInsert < _problem.StopPoints.Length);
                        Debug.Assert(_extraVisitCounter <= _targetExtraDepotVisits);
                        double beforeInsert = _curFilledRoute.Length; 
                        _curFilledRoute.InsertNode(_curFilledRoute.Last, _nextToInsert);
                        double increase = _curFilledRoute.Length - beforeInsert;
                        
                        if (_problem.IsDepot(_nextToInsert))
                            _extraVisitCounter += 1;
                        _totalLengthSoFar += increase;
                        _visitedPoints[_nextToInsert] = true;
                        // Debug.WriteLine("Analizing route : " + _curFilledRoute.ToString());

                        /* check this solution can beat best solution - if not => don't continue this way */
                        Debug.Assert(_totalLengthSoFar >= -0.5);
                        if (_bestSolutionLength.Value < _totalLengthSoFar)
                        {
                            if (PopAndPrepareForNextInsert() == false)
                                break;
                            else
                                goto NewNextInsert;
                        }

                        // ADVANCE NEXT TO INSERT
                        /* if cur route is full */
                        if (_curFilledRoute.NodeCount == lengthSetup.lengths[_curFilledRouteIndex] + 2)
                        {
                            /* solution is complete */
                            if (_curFilledRouteIndex == usedRoutes - 1)
                            {
                                for (int i = 0; i < usedRoutes; i++)
                                    Debug.Assert(_routes[i].NodeCount == lengthSetup.lengths[i] + 2);
                                double candidateLength = _routes.Sum(r => r.Length);
                                if (candidateLength < _bestSolutionLength.Value)
                                {
                                    lock (_syncRoot)
                                    {
                                        // compare again , because previous was not synced 
                                        if (candidateLength < _bestSolutionLength.Value)
                                        {
                                            _bestSolutionLength.Value = candidateLength;
                                            _bestSolution.Value = new DvrpSolutionUnit(_problem, _routes);
                                        }
                                    }
                                }

                                if (PopAndPrepareForNextInsert() == false)
                                    break;
                                else
                                    goto NewNextInsert;
                            }
                            /* move to next route */
                            else
                            {
                                _curFilledRouteIndex += 1;
                                _curFilledRoute = _routes[_curFilledRouteIndex];
                                _nextToInsert = FirstPossibleValue();
                                Debug.Assert(_nextToInsert != -1);
                            }
                        }
                        /* route still needs filling */
                        else
                        {
                            _nextToInsert = FirstPossibleValue();
                            Debug.Assert(_nextToInsert != -1);
                        }
                    }
                NextCombination: ;
                }
            }
        }

        /// <summary>
        /// Prepares for next insertion, setting _nextToInsertNode and removing last-added node.
        /// </summary>
        private bool PopAndPrepareForNextInsert()
        {
            /* pop route 'stack' until we next insertion point is found */
            _nextToInsert = -1;
            while (_nextToInsert == -1)
            {
                /* this route is empty, pop from previous one */
                while (_curFilledRoute.NodeCount <= 2)
                {
                    _curFilledRouteIndex -= 1;
                    if (_curFilledRouteIndex == -1) 
                        return false;
                    else
                        _curFilledRoute = _routes[_curFilledRouteIndex];
                }

                int poppedNode = _curFilledRoute.OneBeforeLastNode;
                if (_problem.IsDepot(poppedNode)) 
                    _extraVisitCounter -= 1;
                _curFilledRoute.RemoveNodeBetween(_curFilledRoute.Last.Previous, _curFilledRoute.Last, false);
                _visitedPoints[poppedNode] = false;
                _nextToInsert = NextPossibleValueHigherThan(poppedNode);
            }
            _totalLengthSoFar = _routes.Sum(r => r.Length);
            Debug.Assert(_totalLengthSoFar > -0.5);
            Debug.Assert(_nextToInsert != -1);
            return true;
        }

        /// <summary>
        /// Returns a stoppoint id of an unvisitedclient or a depot. -1 if there's no next value.
        /// </summary>
        private int NextPossibleValueHigherThan(int curInsertValue)
        {
            for (int nextValue = curInsertValue += 1; nextValue < _visitedPoints.Length; nextValue++)
            {
                /* client */
                if (_problem.IsDepot(nextValue) == false)
                {
                    if (_visitedPoints[nextValue] == true)
                        continue;
                    else
                        return nextValue;
                }
                /* depot */
                else
                {
                    if (_extraVisitCounter < _targetExtraDepotVisits)
                        return nextValue;
                    else
                        continue;
                }
            }

            return -1;
        }

        private int FirstPossibleValue()
        {
            return NextPossibleValueHigherThan(-1);
        }

        //private bool IsFeasibleCombination(int[] routeLengths, int usedRoutes, int depotVisits,
        //    IList<int> startCombination, IList<int> endCombination)
        //{
        //    /* check if each route can return in time */
        //    for (int i = 0; i < usedRoutes; i++)
        //    {
        //        int routeMaxAvailTime = 
        //            _problem.StopPoints[endCombination[i]].availUntil - 
        //            _problem.StopPoints[startCombination[i]].availSince;
        //        if (routeMaxAvailTime < _heuristics.MinimumRouteDuration(routeLengths[i], depotVisits))
        //        {
        //            // Debug.WriteLine("Combination impossible!");
        //            return false;
        //        }
        //    }
        //    return true;
        //}
    }
}
