﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;

namespace TaskSolvers.DVRP
{
    public static class GeneticOperations
    {
        private static Random rand = new Random();

        private const double MUTATION_REMOVED_NODES_MAXIMUM = 0.5;
        private const double MUTATION_MINIMUM_REMOVED_NODES = 0.15;

        /// <summary>
        /// spawns a solution for a problem, using random insertion heuristic (pdfpage 77)
        /// </summary>
        public static DvrpSolutionUnit SpawnSolution(DvrpProblem problem)
        {
            DvrpSolutionUnit spawnling = new DvrpSolutionUnit(problem);
            List<int> nodesToInsert = new List<int>(Enumerable.Range(0, problem.StopPoints.Count()));
            foreach (var depot in problem.DepotIds)
                nodesToInsert.Remove(depot);

            CreateInsertPointsIn(spawnling, problem.VehicleCount);
            RandomlyInsert(nodesToInsert, spawnling);

            return spawnling;
        }

        private static void CreateInsertPointsIn(DvrpSolutionUnit unit, int insertPointsCount)
        {
            for (int i = 0; i < insertPointsCount; i++)
            {
                var route = unit.CreateNewRoute();
                route.InsertFirstLap(GetRandomDepot(unit.Problem), GetRandomDepot(unit.Problem));
            }
        }

        private static void RandomlyInsert(List<int> nodesToInsert, DvrpSolutionUnit unit)
        {
            nodesToInsert = new List<int>(nodesToInsert);

            while (nodesToInsert.Count > 0)
            {
                // select random node to insert
                int randomNode = nodesToInsert[rand.Next(nodesToInsert.Count)];
                nodesToInsert.Remove(randomNode);

                var plausibleInsertPoints = unit.GetAllRouteArcs();
                // remove arcs 'A' where insertion: "A.start->randNode->A.end" violates some constraint
                plausibleInsertPoints.RemoveAll((insertPt) => insertPt.Route.IsFeasibleInsertion(insertPt, randomNode) == false);

                // can insert => insert at the least costy place
                if (plausibleInsertPoints.Count > 0)
                {
                    // find insertion point
                    var leastCostlyInsertionPt = plausibleInsertPoints.MinBy(
                        (ra) =>
                        {
                            var modifiedRoute = ra.Route;
                            return modifiedRoute.GetInsertionCost(randomNode, ra);
                        });

                    // do the insertion
                    var route = leastCostlyInsertionPt.Route;
                    route.InsertNode(leastCostlyInsertionPt, randomNode);
                    unit.UnvisitedNodes.Remove(randomNode);

                    // if insert point used a new lap, create another lap
                    if (unit.Problem.IsLapArc(leastCostlyInsertionPt))
                    {
                        route.InsertNewLap(GetRandomDepot(unit.Problem));
                    }
                }
                // can't insert => add to unassigned nodes
                else
                {
                    unit.UnvisitedNodes.Add(randomNode);
                }
            }
        }

        public static DvrpSolutionUnit CommonNodesCrossover(DvrpSolutionUnit mom, DvrpSolutionUnit dad)
        {
            // count common nodes
            Dictionary<RoutePair, Route> commonNodesMap = new Dictionary<RoutePair, Route>(mom.Routes.Count * dad.Routes.Count);
            foreach (var momRoute in mom.Routes)
            {
                foreach (var node in momRoute.GetIntermediateNodes())
                {
                    var dadRoute = dad.Routes.FirstOrDefault((sRoute) => sRoute.ContainsNode(node));
                    if (dadRoute == null) continue;

                    RoutePair pair = new RoutePair(momRoute, dadRoute);
                    if (commonNodesMap.ContainsKey(pair) == false)
                    {
                        var partiallyCmnRoute = new Route(mom.Problem);
                        partiallyCmnRoute.InsertFirstLap(momRoute.First.From, momRoute.Last.To);

                        commonNodesMap.Add(pair, partiallyCmnRoute);
                    }
                    commonNodesMap[pair].InsertNode(commonNodesMap[pair].Last, node);
                }
            }
            // choose the longest feasible route
            var sortedRoutes = commonNodesMap.OrderByDescending((kvp) => kvp.Value.Count).Select(kvp => kvp.Value);
            var longestFeasibleRoute = sortedRoutes.FirstOrDefault((sRoute) => sRoute.IsFeasible());

            // if no feasible common route => apply mutation
            if (longestFeasibleRoute == null)
            {
                return GeneticOperations.SubsetReinsertionMutation(mom);
            }

            // insert common route into child
            DvrpSolutionUnit child = new DvrpSolutionUnit(mom.Problem);
            child.AddNewRoute(longestFeasibleRoute);

            CreateInsertPointsIn(child, child.Problem.VehicleCount - 1);

            var nodesToInsert =
                mom.Problem.GetClientNodes().
                Except(longestFeasibleRoute.GetIntermediateNodes());
            RandomlyInsert(nodesToInsert.ToList(), child);

            // take 
            return child;
        }

        private struct RoutePair
        {
            public Route momRoute;
            public Route dadRotue;

            public RoutePair(Route momRoute, Route dadRoute)
            {
                this.momRoute = momRoute;
                this.dadRotue = dadRoute;
            }

            public override bool Equals(object obj)
            {
                RoutePair otherPair = (RoutePair)obj;
                return (otherPair.momRoute == this.momRoute) &&
                    (otherPair.dadRotue == this.dadRotue);
            }
            public override int GetHashCode() { return momRoute.GetHashCode() ^ dadRotue.GetHashCode(); }
        }

        public static DvrpSolutionUnit SubsetReinsertionMutation(DvrpSolutionUnit unit)
        {
            return SpawnMultipleMutants(unit, 1)[0];
        }

        public static DvrpSolutionUnit[] SpawnMultipleMutants(DvrpSolutionUnit unit, int populationSize)
        {
            var mutantBase = unit.Clone();

            // remove random amount of nodes
            int nodesToRemoveCount = (int)(Math.Ceiling(unit.Problem.ClientCount * (rand.NextDouble() * MUTATION_REMOVED_NODES_MAXIMUM + MUTATION_MINIMUM_REMOVED_NODES)));

            List<int> nodesToInsert = new List<int>(nodesToRemoveCount + unit.UnvisitedNodes.Count);
            nodesToInsert.AddRange(unit.UnvisitedNodes);

            List<int> removeCandidates =
                Enumerable.Range(0, unit.Problem.StopPoints.Length).
                Except(unit.Problem.DepotIds).
                Except(unit.UnvisitedNodes).
                ToList();

            nodesToRemoveCount = Math.Min(nodesToRemoveCount, removeCandidates.Count);
            for (int i = 0; i < nodesToRemoveCount; i++)
            {
                int node = removeCandidates[rand.Next(removeCandidates.Count)];
                removeCandidates.Remove(node);
                mutantBase.RemoveNode(node);
                nodesToInsert.Add(node);
            }

            // create desired amount of mutants
            DvrpSolutionUnit[] mutants = new DvrpSolutionUnit[populationSize];
            for (int i = 1; i < populationSize; i++)
            {
                mutants[i] = mutantBase.Clone();
                RandomlyInsert(nodesToInsert, mutants[i]);
            }
            RandomlyInsert(nodesToInsert, mutantBase);
            mutants[0] = mutantBase;

            return mutants;
        }

        private static int GetRandomDepot(DvrpProblem problem)
        {
            return problem.DepotIds[rand.Next(problem.DepotIds.Length)];
        }
    }
}
