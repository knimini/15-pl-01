﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TaskSolvers.Utilities;

namespace TaskSolvers.DVRP
{
    /// <summary>
    /// Representation of a DVRP problem.
    /// </summary>
    [Serializable]
    public class DvrpProblem
    {
        #region Properties

        public StopPoint[] StopPoints { get; set; }         // vertices in this problem's 'graph'
        public double[,] DistanceMatrix { get; set; }       // distances between each vertex
        public int VehicleCapacity { get; set; }            // maximum capacity of any vehicle
        public int VehicleCount { get; set; }               // amount of available vehicles
        public double VehicleSpeed { get; set; }            // speed of every vehicle

        public int ClientCount { get; set; }                // number of client vertices (the ones to visit)
        public int[] DepotIds { get; set; }                 // ids of depots (indicies into StopPoints array)

        public bool TimeConstraints { get; set; }           // are we considering arrival constraints:
        // - arrive @ clients after they become available
        // - visit depots only within their time window
        public bool CapacityConstraint { get; set; }        // are we considering limited capacity of vehicles

        #endregion // Properties

        public DvrpProblem() { }

        public double GetLength(RouteArc arc)
        {
            return DistanceMatrix[arc.From, arc.To];
        }

        public IEnumerable<int> GetClientNodes()
        {
            return Enumerable.Range(0, StopPoints.Length).Where((sId) => StopPoints[sId].isDepot == false);
        }

        public bool IsDepot(int stopPointIndex)
        {
            return StopPoints[stopPointIndex].isDepot;
        }

        public bool IsLapArc(RouteArc arc)
        {
            return StopPoints[arc.From].isDepot &&
                StopPoints[arc.To].isDepot;
        }

        public double GetTravelTime(int from, int to) { return DistanceMatrix[from, to] / VehicleSpeed; }
        public double GetTravelTime(RouteArc arc) { return DistanceMatrix[arc.From, arc.To] / VehicleSpeed; }

        public void FillArrivalAndDelay(double departFromStart, ref RouteArc arc1)
        {
            double travelTime = GetTravelTime(arc1);
            arc1.DelayTime = Math.Max(
                0,
                StopPoints[arc1.To].availSince - (departFromStart + travelTime));
            arc1.ArrivalAtDestination = departFromStart + travelTime + arc1.DelayTime;
        }

        public void FillArrivalAndDelay(double departFromStart, ref RouteArc arc1, ref RouteArc arc2)
        {
            double travelTimeToMiddle = GetTravelTime(arc1);
            double travelTimeMidleToEnd = GetTravelTime(arc2);

            arc1.DelayTime = Math.Max(
                0,
                StopPoints[arc1.To].availSince - (departFromStart + travelTimeToMiddle));
            arc1.ArrivalAtDestination = departFromStart + travelTimeToMiddle + arc1.DelayTime;

            double departFromMiddel = arc1.ArrivalAtDestination + StopPoints[arc1.To].unloadDuration;
            arc2.DelayTime = Math.Max(
                0,
                StopPoints[arc2.To].availSince - (departFromMiddel + travelTimeMidleToEnd));
            arc2.ArrivalAtDestination = departFromMiddel + travelTimeMidleToEnd + arc2.DelayTime;
        }

        public double GetArrivalTimeForRoute(double departTime, int startNode, int targetNode)
        {
            double travelTime = GetTravelTime(startNode, targetNode);
            double delay = Math.Max(
                0,
                StopPoints[targetNode].availSince - (departTime + travelTime));
            return departTime + travelTime + delay;
        }

        public double GetArrivalTimeForRoute(double departTime, int startNode, int intermediateNode, int targetNode)
        {
            double travelTimeToMiddle = GetTravelTime(startNode, intermediateNode);
            double travelTimeMiddleToEnd = GetTravelTime(intermediateNode, targetNode);

            double delay1 = Math.Max(
                0,
                StopPoints[intermediateNode].availSince - (departTime + travelTimeToMiddle));
            double arrivalAtMiddle = departTime + travelTimeToMiddle + delay1;

            double departFromMiddel = arrivalAtMiddle + StopPoints[intermediateNode].unloadDuration;
            double delay2 = Math.Max(
                0,
                StopPoints[targetNode].availSince - (departFromMiddel + travelTimeMiddleToEnd));
            return departFromMiddel + travelTimeMiddleToEnd + delay2;
        }

        #region Equals override

        public override bool Equals(object obj)
        {
            DvrpProblem other = (DvrpProblem)obj;
            if (other == null)
                return false;

            // member comparisons
            bool equal = MoreLinq.Array2DEquals(other.DistanceMatrix, this.DistanceMatrix) &&
                VehicleCapacity == other.VehicleCapacity &&
                VehicleCount == other.VehicleCount &&
                ClientCount == other.ClientCount &&
                MoreLinq.ArrayEquals(DepotIds, other.DepotIds);

            if (equal == false)
                return false;

            // stoppoints comparison
            for (int i = 0; i < StopPoints.Length; i++)
            {
                if (StopPoints[i].Equals(other.StopPoints[i]) == false)
                    return false;
            }

            return true;
        }

        public override int GetHashCode() { return DistanceMatrix.GetHashCode(); }

        #endregion // Equals override
    }

    [Serializable]
    public struct StopPoint
    {
        public const int DEFAULT_UNLOAD = 0;
        public const int DEFAULT_AVAIL_SINCE = 0;
        public const int DEFAULT_AVIAL_UNTIL = 0;

        public bool isDepot;
        public int demand;
        public int availSince;
        public int availUntil;
        public int unloadDuration;

        public StopPoint(int demand, bool isDepot)
            : this(demand, DEFAULT_UNLOAD, DEFAULT_AVAIL_SINCE, DEFAULT_AVIAL_UNTIL, isDepot) { }

        public StopPoint(int demand, int unloadDuration, int availsince, int availuntil, bool isDepot)
        {
            this.isDepot = isDepot;
            this.demand = demand;
            this.availSince = availsince;
            this.availUntil = availuntil;
            this.unloadDuration = unloadDuration;
        }
    }
}
