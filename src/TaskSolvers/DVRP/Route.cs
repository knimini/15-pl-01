﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using TaskSolvers.Utilities;

namespace TaskSolvers.DVRP
{
    [DebuggerDisplay("Arcs : {Count}    Length: {Length}")]
    [Serializable]
    public class Route : IEnumerable<RouteArc>
    {
        [NonSerialized]
        private DvrpProblem _problem;

        private RouteArc _head;
        private RouteArc _tail;

        public DvrpProblem Problem { get { return _problem; } set { _problem = value; } }

        /// <summary>Number of Arcs in the route.</summary>
        public int Count { get; private set; }
        /// <summary>Length of the route.</summary>
        public double Length { get; private set; }
        public double TimeAvailable { get; private set; }

        public RouteArc First { get { return _head; } }
        public RouteArc Last { get { return _tail; } }

        public int NodeCount { get { return Count + 1; } }
        public int LastNode { get { return _tail.To; } }
        public int OneBeforeLastNode { get { return _tail.From; } }
        public int FirstNode { get { return _head.From; } }
        public int SecondNode { get { return _head.To; } }

        public Route(DvrpProblem problem)
        {
            Length = 0.0;
            Count = 0;
            _problem = problem;
            _head = _tail = null;
        }

        #region LINKED LIST BEHAVIOUR

        private void AddLast(RouteArc newArc)
        {
            if (Count == 0)
            {
                newArc.Previous = null;
                newArc.Next = null;
                newArc.Route = this;

                _tail = _head = newArc;
            }
            else
            {
                newArc.Previous = _tail;
                newArc.Next = null;
                newArc.Route = this;

                _tail.Next = newArc;
                _tail = newArc;
            }
            Count += 1;
            Length += _problem.GetLength(newArc);
        }

        private void AddFirst(RouteArc newArc)
        {
            if (Count == 0)
            {
                newArc.Previous = null;
                newArc.Next = null;
                newArc.Route = this;

                _tail = _head = newArc;
            }
            else
            {
                newArc.Previous = null;
                newArc.Next = _head;
                newArc.Route = this;

                _head.Previous = newArc;
                _head = newArc;
            }
            Count += 1;
            Length += _problem.GetLength(newArc);
        }

        private void AddBefore(RouteArc someArc, RouteArc newArc)
        {
            if (someArc.Route != this)
                throw new ArgumentException("Add before must be called on items from this list");
            if (someArc == null || newArc == null)
                throw new ArgumentNullException();

            if (someArc == _head)
                _head = newArc;

            newArc.Route = this;
            newArc.Next = someArc;
            newArc.Previous = someArc.Previous;

            if (someArc.Previous != null)
                someArc.Previous.Next = newArc;
            someArc.Previous = newArc;

            Count += 1;
            Length += _problem.GetLength(newArc);
        }

        private void AddAfter(RouteArc someArc, RouteArc newArc)
        {
            if (someArc.Route != this)
                throw new ArgumentException("Add before must be called on items from this list");
            if (someArc == null || newArc == null)
                throw new ArgumentNullException();

            if (someArc == _tail)
                _tail = newArc;

            newArc.Route = this;
            newArc.Next = someArc.Next;
            newArc.Previous = someArc;

            if (someArc.Next != null)
                someArc.Next.Previous = newArc;
            someArc.Next = newArc;

            Count += 1;
            Length += _problem.GetLength(newArc);
        }

        private void Remove(RouteArc arc)
        {
            if (arc == _head)
            {
                _head = arc.Next;
                if (arc.Next == null)
                    _tail = null;
                else
                    arc.Next.Previous = null;
            }
            else if (arc == _tail)
            {
                _tail = arc.Previous;
                arc.Previous.Next = null;
            }
            else
            {
                arc.Next.Previous = arc.Previous;
                arc.Previous.Next = arc.Next;
            }

            Count -= 1;
            Length -= _problem.GetLength(arc);
        }

        private void Clear()
        {
            _tail = null;
            _head = null;
            Count = 0;
            Length = 0;
        }

        #endregion // LINKED LIST BEHAVIOUR

        #region Insertion / Removal

        public void InsertNode(RouteArc splittedArc, int newNode)
        {
            var newArc1 = new RouteArc(splittedArc.From, newNode, splittedArc.Load);
            var newArc2 = new RouteArc(newNode, splittedArc.To, splittedArc.Load);

            Remove(splittedArc);
            if (splittedArc.Previous == null)
                AddFirst(newArc1);
            else
                AddAfter(splittedArc.Previous, newArc1);
            AddAfter(newArc1, newArc2);

            if (Problem.CapacityConstraint)
            {
                int newLoad = _problem.StopPoints[newNode].demand;
                if (newLoad != 0)
                    PropagateNewLoad(newArc1, newLoad);
            }
            if (Problem.TimeConstraints)
            {
                double srcToNewTime = Problem.GetTravelTime(splittedArc.From, newNode);
                double splittedTime = Problem.GetTravelTime(splittedArc);
                double newToDstTime = Problem.GetTravelTime(newNode, splittedArc.To);

                Problem.FillArrivalAndDelay(splittedArc.DepartureFromSource, ref newArc1, ref newArc2);

                // extra time due to new node, could be neutralized by not-delaying on some futher node
                double timeConsumed = newArc2.ArrivalAtDestination - splittedArc.ArrivalAtDestination;
                var curArc = newArc2.Next;
                while (curArc != null && timeConsumed > 0)
                {
                    if (curArc.DelayTime > 0)
                    {
                        double delayReduction = timeConsumed > curArc.DelayTime ?
                            curArc.DelayTime : timeConsumed;

                        curArc.DelayTime -= delayReduction;
                        timeConsumed -= delayReduction;
                    }
                    curArc = curArc.Next;
                }
                // not fully neutralized, then we just end route a bit later
                if (timeConsumed > 0)
                    TimeAvailable -= timeConsumed;
            }

#if DEBUG
            if (_tail.Load > 0)
                throw new Exception("WTF DID JUST HAPPEN?!");
            if (Problem.TimeConstraints)
            {
                Debug.Assert(newArc1.ArrivalAtSource < newArc2.ArrivalAtDestination ||
                             (newArc1.ArrivalAtSource == newArc2.ArrivalAtDestination && newArc1.From == newArc1.To));
                Debug.Assert(newArc2.ArrivalAtSource < newArc2.ArrivalAtDestination ||
                             (newArc2.ArrivalAtSource == newArc2.ArrivalAtDestination && newArc2.From == newArc2.To));
            }
#endif
        }

        public void InsertFirstLap(int startDepot, int endDepot)
        {
#if DEBUG
            if (Problem.StopPoints[startDepot].isDepot == false ||
                Problem.StopPoints[endDepot].isDepot == false)
                throw new Exception("first arc should be a lap arc");
#endif

            int newLoad = _problem.StopPoints[endDepot].demand;
            var firstArc = new RouteArc(startDepot, endDepot, newLoad);
            AddFirst(firstArc);

            if (Problem.TimeConstraints)
            {
                firstArc.ArrivalAtDestination = _problem.GetTravelTime(startDepot, endDepot);
                TimeAvailable = Problem.StopPoints[endDepot].availUntil - firstArc.ArrivalAtDestination;
            }
        }

        public void InsertNewLap(int depotNode)
        {
            var lastArc = new RouteArc(_tail.To, depotNode, 0);

            if (Problem.TimeConstraints)
            {
                lastArc.ArrivalAtDestination =
                    _tail.ArrivalAtDestination + _problem.GetTravelTime(_tail.To, depotNode);
            }
            AddLast(lastArc);
        }

        public bool RemoveNode(int node)
        {
            // find arc to remove
            RouteArc arcA = this.FirstOrDefault((sArc) => sArc.To == node);
            if (arcA == null)
                return false;
            RouteArc arcB = arcA.Next;

            RemoveNodeBetween(arcA, arcB);
            return true;
        }

        public void ClearRoute()
        {
            Clear();
        }

        /// <summary>
        /// Removes node between to arcs, replacing these two arcs with one.
        /// </summary>
        public void RemoveNodeBetween(RouteArc arcA, RouteArc arcB, bool disappearCycle = true)
        {
            Debug.Assert(arcA.To == arcB.From);
            RouteArc replacementArc = new RouteArc(arcA.From, arcB.To, arcB.Load);

            /* ========= HANDLE ARCS FORMING A CYCLE ============ */
            // if these 2 arcs didn't make a cycle, insert new replacement arc
            if (arcA.From != arcB.To ||
                disappearCycle == false)
            {
                AddAfter(arcA, replacementArc);
            }

            // REMOVE 2 ADJACENT ARCS
            Remove(arcA);
            Remove(arcB);

            /* ==================== UPDATE FOR CONSTRAINTS ================ */
            // update load
            if (Problem.CapacityConstraint)
            {
                int removedLoad = _problem.StopPoints[arcA.To].demand;
                PropagateNewLoad(arcA.Previous, -removedLoad);
            }
            // update times
            if (Problem.TimeConstraints)
            {
                Problem.FillArrivalAndDelay(arcA.DepartureFromSource, ref replacementArc);
                double newArrivalTime = replacementArc.ArrivalAtDestination;
                double oldArrivalTime = arcB.ArrivalAtDestination;

                double savedTime = oldArrivalTime - newArrivalTime;

                if (replacementArc != null)
                    replacementArc.ArrivalAtDestination = newArrivalTime;

                // saved time can be neutralized by forcing a delay further up the road
                var curNode = arcB.Next;
                while (curNode != null && savedTime > 0)
                {
                    double possibleSpeedup = curNode.ArrivalAtDestination - Problem.StopPoints[curNode.To].availSince;
                    double forcedDelay = savedTime > possibleSpeedup ?
                            (savedTime - possibleSpeedup) : 0;

                    savedTime -= forcedDelay;
                    curNode.DelayTime += forcedDelay;
                    curNode.ArrivalAtDestination -= savedTime;
                }
                // if it wasn't neutralized fully, we end the route faster
                if (savedTime > 0)
                    TimeAvailable += savedTime;

                Debug.Assert(replacementArc.ArrivalAtSource < replacementArc.ArrivalAtDestination ||
                    (replacementArc.From == replacementArc.To && replacementArc.ArrivalAtSource <= replacementArc.ArrivalAtDestination));
            }
        }

        #endregion // Insertion / Removal

        #region Public utility functions

        public bool ContainsNode(int node)
        {
            foreach (RouteArc arc in this)
            {
                if (arc.From == node)
                    return true;
            }
            return false;
        }

        public IEnumerable<int> GetIntermediateNodes()
        {
            if (Count == 1)
                return Enumerable.Empty<int>();
            else
                return this.Skip(1).Select((sArc) => sArc.From);
        }

        public bool IsFeasible()
        {
            bool capacityViolated = false, timeViolated = false;

            if (Problem.CapacityConstraint)
            {
                capacityViolated = !this.All((sArc) => sArc.Load <= _problem.VehicleCapacity);
            }

            if (Problem.TimeConstraints)
            {
                timeViolated = false;
                foreach (var arc in this)
                {
                    if (/*arrived before time */
                        (arc.ArrivalAtDestination < Problem.StopPoints[arc.To].availSince) ||
                        /*depot is already closed */
                        (Problem.IsDepot(arc.To) &&
                         Problem.StopPoints[arc.To].availUntil < arc.ArrivalAtDestination))
                    {
                        timeViolated = true;
                        break;
                    }
                }
            }

            return !(timeViolated || capacityViolated);
        }

        public bool IsFeasibleInsertion(RouteArc splittedArc, int insertedNode)
        {
            var curArc = splittedArc;

            // ===== CAPACITY CONSTRAINT =====
            if (Problem.CapacityConstraint)
            {
                int maxCapacity = _problem.VehicleCapacity;
                int newDemand = _problem.StopPoints[insertedNode].demand;

                if (newDemand != 0)
                {
                    do
                    {
                        if (curArc.Load + newDemand > maxCapacity)
                            return false;
                        curArc = curArc.Previous;
                    }
                    while (curArc != null &&
                           _problem.StopPoints[curArc.To].isDepot == false);
                }
            }

            // ==== TIME WINDOW CONTRAINTS ====
            if (Problem.TimeConstraints)
            {
                double srcToNewTime = Problem.GetTravelTime(splittedArc.From, insertedNode);
                double splittedTime = Problem.GetTravelTime(splittedArc);
                double newToDstTime = Problem.GetTravelTime(insertedNode, splittedArc.To);

                // time at dst before insertion
                double timeBeforeInsertion = splittedArc.ArrivalAtDestination;

                // time at dst after insertion
                double timeAfterInsertion = Problem.GetArrivalTimeForRoute(
                    splittedArc.DepartureFromSource,
                    splittedArc.From, insertedNode, splittedArc.To);

                // extra time due to new node, could be neutralized by not-delaying on some futher node
                double timeConsumed = timeAfterInsertion - timeBeforeInsertion;
                curArc = splittedArc.Next;
                while (curArc != null && timeConsumed > 0)
                {
                    timeConsumed -= curArc.DelayTime;
                    curArc = curArc.Next;
                }

                // do we have spare time for that extra visit?
                if (TolerantComparer.Less(TimeAvailable, timeConsumed))
                    return false;
            }

            return true;
        }

        public double GetInsertionCost(int insertedNode, RouteArc splittedArc)
        {
            double cost = 0.0;
            int n1 = splittedArc.From;
            int n2 = insertedNode;
            int n3 = splittedArc.To;

            cost += _problem.DistanceMatrix[n1, n2];
            cost += _problem.DistanceMatrix[n2, n3];
            cost -= _problem.DistanceMatrix[n1, n3];
            return cost;
        }

        public Route Clone()
        {
            var clone = new Route(_problem);

            if (Count == 0)
                return clone;

            // copy arcs
            RouteArc[] cloneArcs = new RouteArc[Count];
            RouteArc curArc = this.First; int i = 0;
            do
            {
                cloneArcs[i] = new RouteArc(curArc.From, curArc.To, curArc.Load);
                cloneArcs[i].Route = clone;
                i += 1;
                curArc = curArc.Next;
            }
            while (curArc != null);

            // set arcs' next and previous member
            for (i = 1; i < cloneArcs.Length - 1; i++)
            {
                cloneArcs[i].Next = cloneArcs[i + 1];
                cloneArcs[i].Previous = cloneArcs[i - 1];
            }
            if (cloneArcs.Length > 1)
            {
                cloneArcs[0].Next = cloneArcs[1];
                cloneArcs.Last().Previous = cloneArcs[cloneArcs.Length - 2];
            }

            // put cloned arcs int clone route
            clone._head = cloneArcs.First();
            clone._tail = cloneArcs.Last();
            clone.Length = this.Length;
            clone.Count = this.Count;
            return clone;
        }

        public RouteArc this[int index]
        {
            get
            {
                if (index > Count)
                    throw new IndexOutOfRangeException();
                int count = 0;
                RouteArc curNode = _head;
                while (count != index)
                {
                    curNode = curNode.Next;
                    count += 1;
                }
                return curNode;
            }
        }

        public override string ToString() { return ToString(false); }

        public string ToString(bool includeTimes)
        {
            if (_head == null) return String.Empty;

            StringBuilder sb = new StringBuilder(Count * 3);
            sb.Append(_head.From.ToString());

            // no times : only nodes
            if (includeTimes == false)
            {
                foreach (var arc in this)
                    sb.Append("->" + arc.To.ToString());
            }
            // times + delays etc
            else
            {
                foreach (var arc in this)
                {
                    sb.Append(String.Format("--{0:0.0}+{1:0.0}-->{2} ({3:0.0})",
                        Problem.GetTravelTime(arc),
                        arc.DelayTime,
                        arc.To,
                        arc.ArrivalAtDestination));
                }
            }
            return sb.ToString();
        }

        #endregion // Public utility functions

        private void PropagateNewLoad(RouteArc lastAffectedArc, int newLoad)
        {
            var curArc = lastAffectedArc;
            while (curArc != null &&
                   _problem.StopPoints[curArc.To].isDepot == false)
            {
                curArc.Load += newLoad;
                curArc = curArc.Previous;
            }
        }

        #region Serializable

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            foreach (var arc in this)
                arc.Route = this;
        }

        #endregion // Serializable

        #region IEnumerable

        public IEnumerator<RouteArc> GetEnumerator()
        {
            return new RouteEnumerator(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new RouteEnumerator(this);
        }

        private class RouteEnumerator : IEnumerator, IEnumerator<RouteArc>
        {
            private Route _route;
            RouteArc _current;
            bool _starting;

            public RouteEnumerator(Route route)
            {
                _route = route;
                _starting = true;
            }

            public RouteArc Current { get { return _current; } }
            object IEnumerator.Current { get { return _current; } }

            public void Dispose() { }

            public bool MoveNext()
            {
                if (_starting)
                {
                    _current = _route.First;
                    _starting = false;
                }
                else
                    _current = _current.Next;

                return _current != null;
            }

            public void Reset()
            {
                _starting = true;
            }
        }

        #endregion IEnumerable
    }


}
