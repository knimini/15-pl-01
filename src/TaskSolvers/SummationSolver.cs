﻿using System;
using System.Collections.Generic;
using UCCTaskSolver;

namespace TaskSolvers
{
    /// <summary>
    /// Task solver for summation of integers.
    /// </summary>
    public class SummationSolver : TaskSolver
    {
        private static readonly string PROBLEM_NAME = "ArraySummation";
        int[] _integerData;

        public override string Name { get { return PROBLEM_NAME; } }

        /// <summary>
        /// Constructs a solver for given data.
        /// </summary>
        /// <param name="data">Array integers in binary form</param>
        public SummationSolver(byte[] data) : base(data)
        {
            _integerData = ConvertData(data);
        }

        #region Task Solver Implementation

        // splits array into subarrays to sum
        public override byte[][] DivideProblem(int threadCount)
        {
            byte[][] subproblems;

            // MORE THREADS THAN ELEMENTS TO ADD UP
            if (threadCount > _integerData.Length)
            {
                subproblems = new byte[_integerData.Length][];
                for (int i = 0; i < subproblems.Length; i++)
                    subproblems[i] = BitConverter.GetBytes(_integerData[i]);

                return subproblems;
            }

            // MORE (OR EQUAL) ELEMENTS THAN THREADS
            int subproblemIntCount = _integerData.Length / threadCount;
            int bytesForSubproblem = subproblemIntCount * sizeof(int);
            int remainderBytes = (_integerData.Length % threadCount) * sizeof(int);

            // split data for each subproblem
            subproblems = new byte[threadCount][];
            for (int i = 0; i < threadCount - 1; i++)
            {
                subproblems[i] = new byte[bytesForSubproblem];
                Buffer.BlockCopy(
                    _integerData, 
                    bytesForSubproblem * i,
                    subproblems[i],
                    0,
                    bytesForSubproblem);
            }
            // and for last subproblem include remainder
            subproblems[threadCount - 1] = new byte[bytesForSubproblem + remainderBytes];
            Buffer.BlockCopy(
                _integerData,
                bytesForSubproblem * (threadCount - 1),
                subproblems[threadCount - 1],
                0,
                subproblems[threadCount - 1].Length);


            return subproblems;
        }

        public override byte[] MergeSolution(byte[][] solutions)
        {
            int sum = 0;

            foreach (var solution in solutions)
                sum += BitConverter.ToInt32(solution, 0);

            return BitConverter.GetBytes(sum);
        }


        public override byte[] Solve(byte[] partialData, TimeSpan timeout)
        {
            int[] integers = new int[partialData.Length];
            Buffer.BlockCopy(partialData, 0, integers, 0, partialData.Length);
            
            int sum = 0;
            for (int i = 0; i < integers.Length; i++)
			{
                sum += integers[i];
			}
            return BitConverter.GetBytes(sum);
        }

        #endregion // Task Solver Implementation

        // converts byte array into Int32 array
        private int[] ConvertData(byte[] data)
        {
            int intCount = data.Length / sizeof(Int32);
            int[] result = new int[intCount];
            for (int i = 0; i < intCount; i++)
			{
                result[i] = BitConverter.ToInt32(data, i * sizeof(Int32));
			}

            return result;
        }
    }
}
